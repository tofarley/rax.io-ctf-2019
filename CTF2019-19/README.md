# Logged Out

## description
```
The Empire has updated their session management and
once again we've lost access to crucial intel. What's
worse, they seem to be actively mocking us.

Hit the link below and find out where the data is
being stored. The fate of the entire Rebellion is in
your hands.

[Link](http://23.253.216.111)

CTF2019-19
```

## hint
```
decode the flask session cookie
```

## author
Tim Farley

## write-up

It's a commonly held belief by many flask developers that the session cookie is `encrypted` by using a pseudo-random `secret_key` but in fact, the cookie content is merely `base64` encoded. The secret_key is only used to sign the cookie so that it cannot be easily modified. However, you may find that inexperienced developers attempt to store sensitive information within these cookies thinking that it's a secure place to store their data.

We can find the cookie using your browser's developer console, or using curl with the `--head` option:

```
$ curl --head http://23.253.216.111
HTTP/1.1 302 FOUND
Server: gunicorn/19.9.0
Date: Sun, 24 Mar 2019 18:52:53 GMT
Content-Type: text/html; charset=utf-8
Content-Length: 219
Location: http://23.253.216.111/guess
Vary: Cookie
Set-Cookie: session=eyJmbGFnIjoicmF4Lmlve2YwcnR1bjNfYzAwa2llfWN0ZiIsInRyeV9udW1iZXIiOjF9.XJfSBQ.JYOQEl0e8iiCni20Jpj4680ZN-M; HttpOnly; Path=/
Age: 0
Via: 1.0 ord1fwsg02-wcg.security.rackspace.net
```

The session cookie takes the format: `cookie_data`.`expiration`.`signature` with the cookie itself being base64 encoded. In this example, our cookie is the value `eyJmbGFnIjoicmF4Lmlve2YwcnR1bjNfYzAwa2llfWN0ZiIsInRyeV9udW1iZXIiOjF9` so we'll decode it:

```
$ python
Python 3.6.5 (default, Feb 12 2019, 12:45:16) 
[GCC 4.2.1 Compatible Apple LLVM 10.0.0 (clang-1000.11.45.5)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>> import base64
>>> base64.urlsafe_b64decode('eyJmbGFnIjoicmF4Lmlve2YwcnR1bjNfYzAwa2llfWN0ZiIsInRyeV9udW1iZXIiOjN9')
b'{"flag":"rax.io{f0rtun3_c00kie}ctf","try_number":3}'
>>> 
```

## notes

This one was a bit of a surprise to me too. I was under the assumption that the cookie was better protected than this, but I found this great article while researching how to hack flask cookies, and thought it would make for a fun challenge.

### steps to reproduce

An ansible playbook is included for provisioning a server.

### taking it further

Larger cookies will be automatically compressed. We could create more data so that the cookie needs to be decompressed before reading. We could also try hacking the signature so that we can inject our own cookies into the data.

### links

* https://blog.miguelgrinberg.com/post/how-secure-is-the-flask-user-session
