# Hot on the Trail

## description
```
It seems the Empire has left us a trail of bread
crumbs. Every route on this site only returns more
links to other routes! These Imperial web developers
are as devious as they are evil.

Check the link below and report your findings.

[Link](http://23.253.20.106/)

CTF2019-13
```

## hint
```
curl+jq
```

## author
Tim Farley

## write-up

This challenge makes use of a REST API to provide a json formatted output which contains a URL and token. Following the trail of URLs while providing the token within the allotted timeframe will award you the flag.

This challenge was provided by contributing Racker Jermey Price, and has been used extensively in his Rackspace University (RU) Python training courses.

Full code for this challenge is available at: https://github.com/jeremyprice/RU_Python_IV_API

Sample solution can be found here: https://github.com/jeremyprice/RU_Python_IV_API/blob/master/client.py

Some changes to the original code were implemented by Towne Besel to give the code a more thematic feel for this event.

```
rax.io{f0ll0w_m3_t0_greatness}ctf
```

## notes

I included this challenge because it very closely relates to a regular python exercise of dealing with paginated results when working with RESTful APIs.

### steps to reproduce

Jeremy has provided an ansible playbook which can be used to configure a server to run this challenge via Flask, gunicorn, and nginx proxy: `https://github.com/jeremyprice/RU_Python_IV_API/blob/master/ansible/setup.yml`/

### taking it further

Return additional data to obfuscate the important sections of the output.

### links

* https://github.com/jeremyprice/RU_Python_IV_API
