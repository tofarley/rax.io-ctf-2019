Use Ubuntu 14.04
(tested with Server edition 64-bit)

As root copy all the files into a directory
	/tmp/CTF/
	
cd /tmp/CTF/

bash setup.sh


Notate your IP address.  You may need to add more iptables -j ACCEPT rules for other IP addresses for players.

Takes about 20 minutes on a dual-core VM to compile everything and setup.