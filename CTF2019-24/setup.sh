#!/bin/bash
# Run as root on an Ubuntu 14.04 system
# Run as root on an Ubuntu 14.04 system
# Run as root on an Ubuntu 14.04 system

export DEBIAN_FRONTEND=noninteractive

apt-get update

apt-get install -y \
apache2 \
unzip


# The mysql root database password can just be left blank
# in preference of Unix socket as root login
apt-get install -y \
mysql-server \
mysql-client


# MUST install older unpatched version for Joomla RCE

# curl -O 'http://us1.php.net/distributions/php-5.5.20.tar.bz2'
# Use repo local php-5.5.20.tar.bz2 copy or above command if you need it

tar -xf php-5.5.20.tar.bz2

cd php-5.5.20


apt-get install -y apache2-dev libxml2-dev libssl-dev pkg-config libbz2-dev libcurl4-openssl-dev libjpeg-turbo8-dev libpng-dev


./configure \
	--with-apxs2=/usr/bin/apxs \
	--with-mysql \
	--with-zlib \
	--with-gd \
	--with-jpeg-dir \
	--with-png-dir \
	--with-openssl \
	--with-pcre-regex \
	--with-bz2 \
	--enable-calendar \
	--with-curl \
	--enable-exif \
	--with-imap-ssl \
	--enable-zip \
	--prefix=/usr/local/php5.5

make
make install

cd ..


/usr/local/php5.5/bin/php --version

ls -la /usr/lib/apache2/modules/libphp5.so



# http://www.php.net/manual/en/faq.installation.php#faq.installation.apache2
sudo a2dismod mpm_event
sudo a2enmod mpm_prefork

echo 'AddType application/x-httpd-php .php' > /etc/apache2/conf-enabled/custom-php.conf

service apache2 restart


# older manual way from scratch
#echo "create database joomla;
#GRANT ALL ON joomla.* TO 'joomla'@'localhost' identified by 'KHdzkNXWxeGkbLBa5Uz8';
#flush privileges;
#show databases;" | mysql -u root
# if needed to try again
#	echo 'drop database joomla;' | mysql -u root


# Newer way
#	Created with command:  mysqldump --routines --databases mysql joomla > joomla_mysqldump.txt

mysql -u root  < joomla_mysqldump.txt

# MySQL 5.5 does not include by default
echo "[mysqld]" > /etc/mysql/conf.d/auth_socket.cnf
echo "plugin-load=auth_socket.so" >> /etc/mysql/conf.d/auth_socket.cnf

service mysql restart




# Restrict access to NOT internet but only CTF players
# Rackspace sees this Joomla exploit attack literally every week against us
# Don't want some random bot on the Internet popping the box
iptables -I INPUT -s 108.166.30.189 -j ACCEPT
iptables -I INPUT -s 173.203.4.161 -j ACCEPT
iptables -I INPUT -s 192.168.0.0/16 -j ACCEPT
iptables -I INPUT -s 10.0.0.0/8 -j ACCEPT
iptables -I INPUT -s 172.16.0.0/12 -j ACCEPT

iptables -A INPUT -p tcp --dport 80 -j REJECT

iptables -L -vn
#	!!! ADD IPS AS NEEDED	!!!



#curl --output /var/www/html/dirb--Buster_Keaton_as_a_bellboy_in_the_March_18_1918_movie_The_Bell_Boy.gif   https://upload.wikimedia.org/wikipedia/commons/a/a0/Buster_Keaton_as_a_bellboy_in_the_March_18_1918_movie_The_Bell_Boy.gif
cp dirb--Buster_Keaton_as_a_bellboy_in_the_March_18_1918_movie_The_Bell_Boy.gif /var/www/html/


echo '<img src="dirb--Buster_Keaton_as_a_bellboy_in_the_March_18_1918_movie_The_Bell_Boy.gif">' > /var/www/html/index.html



tar -xf ctf-joomla.tar.bz2 --directory /var/www/html/

# You should use the tar.bz2 pre-configured Joomla as it already has the configuration options.
# Otherwise see the references to download version 3.4.1
# Run the installation as appropriate.


chown -R www-data:www-data /var/www/html/
chmod -R u+w,go+r /var/www/html/

service apache2 restart
service mysql restart


echo 'rax.io{CustomersReallyDontPatchSince2015}ctf' > /flag
chmod 0444 /flag
chown root:root /flag


# Authorized Rackspace CTF participants only.


# Misc references (not always 100% correct directions)
#	https://www.linuxtechi.com/installation-steps-joomla-3-4-1-on-ubuntu-14-10/
#	https://opensourcehacker.com/2010/12/21/creating-mysql-database-and-user-from-command-line/
#	https://launchpad.net/ubuntu/+source/php5/5.5.9+dfsg-1ubuntu4.12
#	https://launchpad.net/ubuntu/+source/php5/5.5.9+dfsg-1ubuntu4.13
#	https://downloads.joomla.org/us/cms/joomla3
#	https://downloads.joomla.org/us/cms/joomla3/3-4-1/joomla_3-4-1-stable-full_package-tar-bz2?format=bz2

# Solution hints
#	The index.html already tells them to dirb
#
#	Also players should figure out themselves the Joomla version via
#		http://104.130.217.19/joomla/administrator/language/en-GB/en-GB.xml
#
#	Players should figure out themselves
#		https://www.rapid7.com/db/modules/exploit/multi/http/joomla_http_header_rce
