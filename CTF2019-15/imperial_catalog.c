#include <stdio.h>
#include <string.h>

#define HIDE_LETTER(a)   (a) + 0x50
#define UNHIDE_STRING(str)  do { char * ptr = str ; while (*ptr) *ptr++ -= 0x50; } while(0)
#define HIDE_STRING(str)  do {char * ptr = str ; while (*ptr) *ptr++ += 0x50;} while(0)
int main()
{   
    char text[16];

    printf("  Welcome to\n");

    printf("  _____ _          ___            _         \n");
    printf(" |_   _| |_  ___  | __|_ __  _ __(_)_ _ ___ \n");
    printf("   | | | ' \\/ -_) | _|| '  \\| '_ \\ | '_/ -_)\n");
    printf("   |_| |_||_\\___| |___|_|_|_| .__/_|_| \\___|\n");
    printf("                            |_|             \n");

    // store the "secret password" as mangled byte array in binary
	char pass[] = { HIDE_LETTER('j') , HIDE_LETTER('o') , HIDE_LETTER('i') , HIDE_LETTER('n') , HIDE_LETTER('t') 
	, HIDE_LETTER('h') , HIDE_LETTER('e') , HIDE_LETTER('d') , HIDE_LETTER('a') , HIDE_LETTER('r'),
		HIDE_LETTER('k') ,HIDE_LETTER('s') ,HIDE_LETTER('i') ,HIDE_LETTER('d') ,HIDE_LETTER('e'),'\0' }; 

    printf("Enter secret Imperial codes: ");
    scanf("%17s", text);

    if(strcmp(pass, text)==0)
    {
        printf("rax.io{t00_ea$y}ctf");
    }
    else
    {
        printf("\nBetter luck next time, Rebel scum!\n");
        printf("They won't all be this easy!\n");
    }

    return 0;
}