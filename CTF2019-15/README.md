# Imperial Catalog

## description
```
This is a binary catalog detailing important Imperial
activity. Unfortunately it's protected by a passcode
of some sort. As usual, we're useless in situations
like this. We've provided copies of the application
in both Windows and Linux binary formats.

[imperial_catalog](http://bfade9c78928fb77218e-10b8dc0045fa3923b9f34ee97dd352fb.r99.cf5.rackcdn.com/CTF2019-15/imperial_catalog)

[imperial_catalog.exe](http://bfade9c78928fb77218e-10b8dc0045fa3923b9f34ee97dd352fb.r99.cf5.rackcdn.com/CTF2019-15/imperial_catalog.exe)

CTF2019-15
```

## hint
```
strings
```

## author
Tim Farley

## write-up

This is about as dead simple as it gets. You're provided a binary in both Windows PE and Linux ELF format.

The first thing we always do when downloading an unknown binary for cracking is to extract the ASCII strings in the file and/or determine the file type with `file`. In most real-world applications, `strings` won't provide us a direct answer, but it may very well provide clues, such as types of system calls we might expect to see in the application.

Windows users may want to download `strings.exe` from https://docs.microsoft.com/en-us/sysinternals/downloads/strings

Running strings, we see:


```
$ strings imperial_catalog
<<TRUNCATED>>
Enter secret Imperial codes: 
%17s
rax.io{t00_ea$y}ctf
Better luck next time, Rebel scum!
They won't all be this easy!
;*3$"
GCC: (Debian 8.2.0-14) 8.2.0
<<TRUNCATED>>
```

Which yields:

```
rax.io{t00_ea$y}ctf
```

## notes

A simple challenge to get you into the habit of inspecting those binaries!

### steps to reproduce

Compile the provided code:

Linux:
```
gcc -o imperial_catalog imperial_catalog.c
```

Windows (Visual Studio 2017):
```
cl imperial_catalog.c
```

### taking it further

Check out the links below for information on obfuscating strings in C.

### links

* https://stackoverflow.com/questions/1356896/how-to-hide-a-string-in-binary-code
* https://yurisk.info/2017/06/25/binary-obfuscation-string-obfuscating-in-C/index.html
* https://docs.microsoft.com/en-us/sysinternals/downloads/strings
