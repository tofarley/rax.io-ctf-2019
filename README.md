# rax.io_ctf_2019
A repository of CTF challenges created for the rax.io conference 2019.


## about
My name is Tim Farley. I have been working with Linux and UNIX systems for over twenty years both professionally and as a hobby. In 2017 I joined the Rackspace Managed Security Defensive Infrastructure team, and in 2018 I co-founded and was subsequently appointed Event Coordinator and Captain of my very own Capture the Flag (CTF) team comprised of Rackers, former Rackers, and friends. I was flattered when the rax.io CTF planning committee invited me to be the head of content for this year's event.

In this repository you will find all of the various challenges, corresponding write-ups, and necessary information to recreate these challenges for future events, should you so desire. I have also included a little background on each event including where I drew inspiration, what I hoped to achieve with a particular challenge, and any relavent links that helped me during creation.

One of the unique challenges surrounding this particular event was that the bulk of the contestants would be located physically at Rackspace Headquarters and would thus be subject to corporate network policies. In an effort to be inclusive to geographically separated players and to court interested parties who may not be in technical roles, I decided very early that this event would strive for a zero-infrastructure environment that would not be limited by, or give advantage to, any players based on their proximity to the Rackspace infrastructure.

Past rax.io CTF events made use of `pods` which were deployed in advance of the game for each team, and provided each team access to a private internal network. As I joined this project only 6 short weeks before rax.io, I opted to do away with this concept in favor of a more streamlined approach more inline with what I've experienced in recent competitions. I didn't want players to spend the first thirty minutes of the event sorting out credentials and becoming familiar with our prescribed environment. I wanted you to be able to start attacking the challenges in whatever manner you were most comfortable and thus, with the exception of a few web servers running in Rackspace Cloud, every challenge was distributed as a stand-alone file hosted on the Rackspace Cloud Files infrastructure.

I hope you enjoy the new format and appreciate all the work that went into making this event a success. Thanks for playing!
Tim


# rackspace_ctf

Many challenges are now managed via ansible playbook.

We utilize the rax module of ansible to automatically create challenge servers and deploy to them.

Inside the `ansible` directory you will find a script called `ansible-setup.py` which will configure a virtualenv with the required python modules, download the SSH private key from passwordsafe, and prepare your local computer.

Features:
* Automatically create `n` challenge servers with incremental names beginning at `content-001`.
* Automatically build servers to specification as per the challenge.
* Servers are automatically configured using an ssh keypair stored in the cloud. The setup script will download the private key from passwordsafe, and you can connect with `ssh -i ~/.ssh/id_rsa_rax root@IP`
