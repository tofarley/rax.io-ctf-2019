# gothub

## description
```
Look alive! We just found a public repository filled
with Imperial code and it looks like there may have
been a potential password leak for non-other than
Vader himself!

[deathstar_plans](https://github.com/tofarley/deathstar_plans)

CTF2019-07
```

## hint
```
curl https://api.github.com/repos/tofarley/deathstar_plans/events
```

## author
Tim Farley

## write-up

This challenge examines the idea that it's *really* hard to scrub data from a git repo. Doubly so when that data is stored on a third party provider, such as github.

Let's start by cloning the repository that was linked in the challenge and having a look around:

```
$ git clone https://github.com/tofarley/deathstar_plans.git
Cloning into 'deathstar_plans'...
remote: Enumerating objects: 9, done.
remote: Counting objects: 100% (9/9), done.
remote: Compressing objects: 100% (6/6), done.
remote: Total 9 (delta 0), reused 6 (delta 0), pack-reused 0
Unpacking objects: 100% (9/9), done.
tofarley@kali:~$ cd deathstar_plans/
tofarley@kali:~/deathstar_plans$ ls
DeathStar1.jpg  README.md
```

We can have a look at DeathStar1.jpg but it's really just a red-herring and there to make the repo a little more thematic. Looking at the README reveals something of interest:

```
# deathstar_plans

\`\`\`
admin user: lord_vader
admin pass: <no passwords in github>
\`\`\`

```

This, coupled with the hint in the description, tells us that there is likely a previous commit in which a password existed inside this file. Let's check our commit history:

```
$ git log --all
commit d49ccd46faefcb7f9f163763eddee3233f69f602 (HEAD -> master, origin/master, origin/HEAD)
Author: Tim Farley <tim.farley@rackspace.com>
Date:   Wed Mar 13 19:58:58 2019 -0500

    added plans

commit 4424c90e4f1b1cf76b0c3d5308a1734886208c22
Author: Tim Farley <tim.farley@rackspace.com>
Date:   Wed Mar 13 19:37:40 2019 -0500

    fixed

commit a2a172c552ab9942251cd486ab8d95b71ac2070b
Author: Tim Farley <tim.farley@rackspace.com>
Date:   Wed Mar 13 19:30:52 2019 -0500

    Initial commit
```

Unfortunately, there's nothing of value in the git log. We can verify each commit by checking them out into their own branch using the commit SHA, for example: `git checkout -b fixed 4424c90e4f1b1cf76b0c3d5308a1734886208c22`.

This creates a new local branch called `fixed` at commit `4424c90e4f1b1cf76b0c3d5308a1734886208c22` but there's nothing to see here.

So where did that commit go? We could start by checking for tags, branches, or other refs, but in this case there doesn't seem to be much information available.

In all likelihood that commit was `rebased` by a developer. A rebase is a common practice used to squash a series of commits down into one single commit. This could be used to keep the commit history clean after code has gone through a lengthy review process, or it could be unwittingly used by a programmer to try and hide a career-ending mistake!

So we are pretty sure that a commit existed at some point. But it was rebased out of the final code. What if the developer had accidentally pushed that commit to github before realizing his mistake? Is it possible that github itself might hold a reference to the missing commit(s)?

Checking the github web page doesn't produce much information either. So let's check the events feed via the API:

```
curl https://api.github.com/repos/tofarley/deathstar_plans/events | jq . > github_api_response.json
```

Here wew use `curl` and `jq` to create a json file containing the API output. I have included this file in the repo for your review.

Now let's use jq to extract each of the `commits`:

```
$ jq -r '.[] | .payload | .commits[]? | .sha' github_api_response.json
d49ccd46faefcb7f9f163763eddee3233f69f602
4424c90e4f1b1cf76b0c3d5308a1734886208c22
5e892021ed3a7b079dcfb12ee7c2308ceda18e25
``` 

`jq` can be a little daunting, so I'll break this down for you. `-r` runs jq in raw mode. Without this option, each of our results would be surrounded in quotes. No big deal. By looking at the format of the file we can see that the response from the api consisted of a list (`[]`) at the top level (`.`), and inside each list was a key called `payload`. Inside each payload was another list with a key called `commits`, although `commits` doesn't exist in every-single entry so we use a `?` after the list to tell jq to only return results that *do* have this field. Finally, inside each commit there is an `sha` field. This is the sha of each individual commit, and that's what we're looking for.

Thus the command breaks down as follows

```
jq -r '.[] | .payload | .commits[]? | .sha'
^
command
   ^
   raw mode (used to remove the quotes from the results in our case)
       ^
       top-level list. jq uses a basic structure where everything exists in inside '.[]'
       in our case, we will pipe that result into further filters to return the data we seek
                                  ^
                                  only return entries that contain this field, and not null.
```

Whew. Easy, right?

At any rate, if we check the value of the commits, we can see there is one commit that github knows about that git log has been keeping from us. Commit `5e892021ed3a7b079dcfb12ee7c2308ceda18e25`.

let's just pop open that specific commit in the old browser:

```
https://github.com/tofarley/deathstar_plans/commit/5e892021ed3a7b079dcfb12ee7c2308ceda18e25
```

And we have our flag!

```
- # deathstar_plans
+ # deathstar_plans

+ \`\`\`
+ admin user: lord_vader
+ admin pass: rax.io{pr0tect_y0ur_p@ssw0rds!}ctf
+ \`\`\`
```

## notes

I originally set out to replicate one of the challenges I found in the bandit challenges on overthewire.org. In this challenge, the answer could be found using `git log --all` but as I was creating the challenge, I stumbled across a post on stackoverflow that changed my direction.

### steps to reproduce

Create a repository on github. In this case we will call it `raxio2019`, because why not?

In short, we are going to clone a local copy of this repo, push some bad changes, and then rebase-away those bad changes, like so:

```
$ git clone git@github.com:tofarley/raxioctf2019.git
$ cd raxioctf2019
$ vi README.md # Here we accidentally write some sensitive data.
$ git add README.md
$ git commit -am "added user"
$ git push     # And now we've pushed sensitive data to github!
$ vi README.md # Let's fix our mistake!
$ git add README.md
$ git commit -am "fixed"
$ git rebase -i HEAD~2  # Here we rebase onto an older commit,
# selecting the new commit for use.
# When rebasing, we will have to manually fix any conflicts.
$ vi README.md  # Fixing the conflicts.
$ git add README.md 
$ git rebase --continue # Continue with the rebase after fixing conflicts.
$ git push --force
```

### taking it further

I'm pretty happy with this challenge, as I've not encountered anything quite like it in other CTFs. I'd be happy to hear other ways it could be improved, aside from adding additional commits/branches/tags to make the search more difficult.

I hope you enjoyed it!

### links

* https://developer.github.com/v3/activity/events/
* https://markhneedham.com/blog/2015/10/09/jq-error-cannot-iterate-over-null-null/