# Carving out a Piece

## description
```
Our agent inside Jabba's palace managed to smuggle
this hard drive out from his accounting room.
Unfortunately, someone tipped him off that we were
onto him and it looks like they've deleted all their
data. Let's see if there's anything recoverable. In
particular we're looking for incrementing PNGs of
Jabba and his Imperial connections. See if you can
carve something out of this.

[Recovery.img](http://bfade9c78928fb77218e-10b8dc0045fa3923b9f34ee97dd352fb.r99.cf5.rackcdn.com/CTF2019-05/Recovery.img)

CTF2019-05
```

## hint
```
https://github.com/sleuthkit/scalpel/pull/27/commits/f7ece42aed0950775ebbf53140ae7c4e0f6608e0
```

## author
Tim Farley

## write-up

There are a number of data recovery tools available for both Windows and Linux. I decided to use `scalpel` as it's widely available and well documented.

By default, the scalpel configuration file at `/etc/scalpel/scalpel.conf` has all the various extraction modes disabled. We can edit the file and remove the comment ahead of the following line:

```
png	y	20000000	\x50\x4e\x47?	\xff\xfc\xfd\xfe
```

The fun thing here is that this is really not the correct header or footer format for a PNG file. The first 8 bytes of a valid png will always be `\x89\x50\x4e\x47\x0d\x0a\x1a\x0a`. The format shown in `scalpel.conf` *may* find a file to retrieve, but it will be missing the first byte of the file, and scalpel will have to guess where the file ends, resulting in a corrupt PNG that requires further corrections before it's visible in most viewers.

Interestingly, this issue with the scalpel config file was discovered and reported back in July 2017, but the PR was never merged.

Of course, if you used some other tool to recover the file, then all of this may have been lost on you. :)

Anyways, we can insert our new header into our own custom conf file like so:

```
echo "png y 20000000 \x89\x50\x4e\x47\x0d\x0a\x1a\x0a \x00\x00\x00\x00\x49\x45\x4e\x44\xae\x42\x60\x82" > ./scalpel.conf
scalpel ./Recovery.img -o recovery -c ./scalpel.conf
eog recovery/png-0-0/00000000.png
```

The commands above create a new scalpel.conf and then use that configuration to extract files from `Recovery.img` into the `recovery/` directory. The image is a QR code:

![flag qr](flag.png)

This QR code contains our flag:

```
rax.io{rec0ver3d_da7a}ctf
```

## notes

The purpose here is to show that even the proper tool for the job is useless if you don't understand how it works. I sort of stumbled across this long-standing PR and thought it would make a nice medium-level challenge. 

### steps to reproduce

Create a disk image of any common filesystem type. Mount the new filesystem and then write a file to it. Delete that file and unmount the system. Export the disk as an image file for distribution.

### taking it further

-

### links

* http://www.libpng.org/pub/png/spec/1.2/PNG-Structure.html
* https://github.com/sleuthkit/scalpel/pull/27/commits/f7ece42aed0950775ebbf53140ae7c4e0f6608e0
* http://www.linux-magazine.com/Online/Features/Recovering-Deleted-Files-with-Scalpel
* http://www.panozzaj.com/blog/2011/08/01/using-scalpel-to-recover-lost-data-in-ubuntu/