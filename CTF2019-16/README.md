# Imperial Application

## description
```
This is a binary application detailing important Imperial activity.
Unfortunately it's protected by a passcode of some sort. You've done
so well thus far. Have a crack at this one. We've provided copies of
the application in both Windows and Linux binary formats.

[imperial_application](http://bfade9c78928fb77218e-10b8dc0045fa3923b9f34ee97dd352fb.r99.cf5.rackcdn.com/CTF2019-16/imperial_application)

[imperial_application.exe](http://bfade9c78928fb77218e-10b8dc0045fa3923b9f34ee97dd352fb.r99.cf5.rackcdn.com/CTF2019-16/imperial_application.exe)

CTF2019-16
```

## hint
```
You'll probably want a debugger/interactive disassembler for this one.
```

## author
Tim Farley

## write-up

### windows

We give a lot of love to Linux in these sort of events, so let's do this one in Windows.

This is a continuation on the `imperial_catalog` challenge. This time the passphrase and flag are a little harder to find.

We start as we might with any challenge, by checking for strings. I recommend the `strings.exe` utility from Sysinternals.

Unfortunately, this time, strings doesn't offer us much information. We can also try a utility such as `pestudio` to find a little more information about the binary, but honestly it doesn't give us much to work with in this example.

So let's run the program a few times from the command prompt and see how it behaves. When we launch the program it says `Welcome to The Empire`, displays some super sweet ascii art, and then prompts us with `Enter the secret Imperial codes:`

![application](img/olly0.png)

Not ominous at all.

If we attempt to enter a code at random, the program warns, `Better luck next time, Rebel scum!` and the application exits.

Let's open this thing up in a debugger. On Windows you could use the official Visual Studio `Windbg`, `IDA Pro`, or even the NSA's newly released `ghidra`, but I've chosen to use `Ollydbg`.

From Ollydbg choose `File / Open` and select `imperial_application.exe`. You should see a number of windows open up, including a standard blank command prompt.

![olly loaded](img/olly1.png)

Olly has generated a default breakpoint on program entry. We can click the triangular blue `Run program` button a few times and watch the console output until we see the expected prompt:

![input prompt](img/olly2.png)

Looking through the code we notice something interesting. We see a section of code that says, "Welcome to" followed by a bunch of random `puts` commands that looks vaguely like our ascii art:

![ascii art in binary](img/olly3.png)

Following that, we see a bunch of data being moved around via `MOV BYTE PTR`. This is suspicious and could very well be the part of code where our passphrase and/or flag are being decoded.

We continue scrolling past this funny bit, and we find another *very* interesting section:

![revealing code](img/olly4.png)

So we see plain ascii that reads `Enter secret Imperial codes:` followed by a `scanf`, which google tells us is used for reading input from the console.

Not long after, we find `strcmp`. Again, a quick google search tells us that this function is used to compare two strings for equality. Immediately following the strcmp are the functions `TEST` and `JNZ` (Jump if not Zero).

So, the program prompts us to enter a secret passphrase (printf) then reads ASCII for `%17s` characters (scanf), and then compares the results against some value (strcmp), and finally, jumps if the result of that comparison is not `ZERO`.

What if we told the program that instead of jumping `if not zero`, that it should jump `if zero`. Hypothetically then, we would invert the logic and the program would assume that we'd entered the right passphrase whenever the wrong passphrase was entered, and vice versa!

We google `x86 jnz`. The first result I got was http://unixwiz.net/techtips/x86-jumps.html. From this page, we can see that jnz is represented in hexadecimal by opcode `75`. Let's look again at the line above, we can find this value:

![jnz](img/olly5.png)

We see the hex value of this field is `75 66`. We also note from our internet search that the opposite command, `JZ` (Jump if Zero) is hexadecimal `74` (Jump if Zero / Jump if Equal).

In Ollydbg, we can edit the binary live, before our code ever reaches the instructions! Right click on the line above and choose `Binary / Edit`:

![edit](img/olly6.png)

Replace the hexadecimal `75` for `JNZ` with the value `74` representing `JZ/JE`.

The new code reads:

![fixed](img/olly7.png)

With all that in place, let's move over to the pending command prompt. At this point we should be able to type just about anything into the terminal and get our flag!

![success](img/olly8.png)

Success! We have our flag:

```
rax.io{r3vrs1nglyf3}ctf
```


### linux

Now that we've gotten Windows out of the way, let's look for another way to crack this app. Of course we could attack it exactly the same way using `gdb`, but let's try a completely different tact.

If you recall in the windows section, we noticed that after printing the ascii art, but before requesting the Imperial codes, there was a big block of code that seemed to do nothing but move a bunch of data around in memory. We mused that this could be a point in the code where the secret code and/or the flag was being decrypted. If that is the case, then by the time we are prompted to enter the code, we may have data already decoded in memory.

Let's test that by running the program and then dumping the running memory to a file.

Start up the application in one terminal and let it sit at the prompt. In another terminal session, run:

```
# gcore $(pgrep imperial)
BFD: /usr/lib/debug/.build-id/55/e7de84df6813c82c5b12822219c17d19c9c4b0.debug: unable to initialize decompress status for section .debug_aranges
BFD: /usr/lib/debug/.build-id/55/e7de84df6813c82c5b12822219c17d19c9c4b0.debug: unable to initialize decompress status for section .debug_aranges
warning: File "/usr/lib/debug/.build-id/55/e7de84df6813c82c5b12822219c17d19c9c4b0.debug" has no build-id, file skipped
BFD: /usr/lib/debug/.build-id/75/5312dcb2382eb2fde78494879bb2104028ae80.debug: unable to initialize decompress status for section .debug_aranges
BFD: /usr/lib/debug/.build-id/75/5312dcb2382eb2fde78494879bb2104028ae80.debug: unable to initialize decompress status for section .debug_aranges
warning: File "/usr/lib/debug/.build-id/75/5312dcb2382eb2fde78494879bb2104028ae80.debug" has no build-id, file skipped
0x00007f04d2836761 in read () from /lib/x86_64-linux-gnu/libc.so.6
Saved corefile core.6571
[Inferior 1 (process 6571) detached]
```

Here we use `pgrep` to find the pid of our running application, and `gcore` to dump that memory out to corefile `core.6571` on disk.

We can use `hexdump` or `xxd` to inspect the data, but what's the first thing I always like to do when faced with a new file? `strings`!

```
# strings core.6571
<<TRUNCATED>
xeon_phi
linux-vdso.so.1
tls/haswell/x86_64/tls/x86_64/
/lib/x86_64-linux-gnu/libc.so.6
////////////////
Enter secret ImpImperial codes: 
////////////////
er secret Imperi
jointhedarkside
- 9b
1x86_64
<<TRUNCATED>>
```

Well there's a string we didn't see before! `jointhedarkside`.

Is there any chance at all that this is the super secret code that we've heard so much about?

```
# ./imperial_application 
  Welcome to
  _____ _          ___            _         
 |_   _| |_  ___  | __|_ __  _ __(_)_ _ ___ 
   | | | ' \/ -_) | _|| '  \| '_ \ | '_/ -_)
   |_| |_||_\___| |___|_|_|_| .__/_|_| \___|
                            |_|             
Enter secret Imperial codes: jointhedarkside

rax.io{r3vrs1nglyf3}ctf
```

We found our flag! Interesting to note here is that when I created this application, I had no intention of cracking it this way. I only realized this flaw while I was inspecting the code in Ollydbg and noticed that big block of strange data being moved around after the ascii art, but before the user was prompted for input. I then realized that I had decoded the passphrase even before prompting the user, meaning that the code was just sitting there in memory totally unprotected. The source:

```
	UNHIDE_STRING(pass);  // unmangle the string in-place
    printf("Enter secret Imperial codes: ");
    scanf("%17s", text);

    if(strcmp(pass, text)==0)
    {
        UNHIDE_STRING(flag);
        printf("\n%s\n", flag);
        HIDE_STRING(flag);
    }
    else
    {
        printf("\nBetter luck next time, Rebel scum!\n");
    }
```

We could have made the challenge a little more difficult by only decoding the string in the very moment that it was needed, but even so it would not have completely prevented us from getting the code.

## notes

The old `jump if equal` / `jump if not equal` trick is a classic. I first learned this technique circa 2004 and it served me surprisingly well. Many modern programs will add a lot of additional checks to ensure that this is defeated, but it remains a useful and valid skill to this day.

### steps to reproduce

Compile the provided code:

Linux:
```
gcc -o imperial_application imperial_application.c
```

Windows (Visual Studio 2017):
```
cl imperial_application.c
```

### taking it further

Check out the links below for information on obfuscating strings in C.

### links

* https://stackoverflow.com/questions/1356896/how-to-hide-a-string-in-binary-code
* https://yurisk.info/2017/06/25/binary-obfuscation-string-obfuscating-in-C/index.html
* https://docs.microsoft.com/en-us/sysinternals/downloads/strings
* http://www.cplusplus.com/reference/cstdio/scanf/
* http://www.cplusplus.com/reference/cstring/strcmp/
* http://ref.x86asm.net/coder32.html
* https://serverfault.com/questions/173999/dump-a-linux-processs-memory-to-file