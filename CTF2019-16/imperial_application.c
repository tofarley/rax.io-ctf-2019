#include <stdio.h>
#include <string.h>

#define HIDE_LETTER(a)   (a) + 0x50
#define UNHIDE_STRING(str)  do { char * ptr = str ; while (*ptr) *ptr++ -= 0x50; } while(0)
#define HIDE_STRING(str)  do {char * ptr = str ; while (*ptr) *ptr++ += 0x50;} while(0)
int main()
{   
    const char *alpha[2] = { "abcdefghijklmnopqrstuvwxyz", "ABCDEFGHIJKLMNOPQRSTUVWXYZ"};
    
    char text[16];

    printf("  Welcome to\n");

    printf("  _____ _          ___            _         \n");
    printf(" |_   _| |_  ___  | __|_ __  _ __(_)_ _ ___ \n");
    printf("   | | | ' \\/ -_) | _|| '  \\| '_ \\ | '_/ -_)\n");
    printf("   |_| |_||_\\___| |___|_|_|_| .__/_|_| \\___|\n");
    printf("                            |_|             \n");

    // store the "secret password" as mangled byte array in binary
	char pass[] = { HIDE_LETTER('j') , HIDE_LETTER('o') , HIDE_LETTER('i') , HIDE_LETTER('n') , HIDE_LETTER('t') 
	, HIDE_LETTER('h') , HIDE_LETTER('e') , HIDE_LETTER('d') , HIDE_LETTER('a') , HIDE_LETTER('r'),
		HIDE_LETTER('k') ,HIDE_LETTER('s') ,HIDE_LETTER('i') ,HIDE_LETTER('d') ,HIDE_LETTER('e'),'\0' }; 

	char flag[] = { HIDE_LETTER('r') , HIDE_LETTER('a') , HIDE_LETTER('x') , HIDE_LETTER('.') , HIDE_LETTER('i') 
	, HIDE_LETTER('o') , HIDE_LETTER('{') , HIDE_LETTER('r') , HIDE_LETTER('3') , HIDE_LETTER('v'),
		HIDE_LETTER('r') ,HIDE_LETTER('s') ,HIDE_LETTER('1') ,HIDE_LETTER('n') ,HIDE_LETTER('g'),HIDE_LETTER('l'),HIDE_LETTER('y'),HIDE_LETTER('f'),HIDE_LETTER('3'),HIDE_LETTER('}'),HIDE_LETTER('c'),HIDE_LETTER('t'),HIDE_LETTER('f'),'\0' }; 

	UNHIDE_STRING(pass);  // unmangle the string in-place
	//printf("Here goes the secret we hide: %s\n", pass);
    printf("Enter secret Imperial codes: ");
    scanf("%17s", text);
	//printf("Here goes the secret we got: %s\n", text);

    if(strcmp(pass, text)==0)
    {
        UNHIDE_STRING(flag);
        printf("\n%s\n", flag);
        HIDE_STRING(flag);
    }
    else
    {
        printf("\nBetter luck next time, Rebel scum!\n");
    }
    HIDE_STRING(pass);  //mangle back

    printf("\n");
    return 0;
}