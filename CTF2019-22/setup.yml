---
  - name: Configure server
    hosts: content-07
    remote_user: root

    vars:
      packages:
        - python3-pip
        - authbind
        - knockd
        - socat
        - curl
      install_directory: /var/app/challenge
      pid_directory: /run/challenge
      runtime_user: www-data
      pid_file: 
      gunicorn_sock: /run/py4class/socket
      gunicorn_port: 8000
      listen_port: 80
      ansible_python_interpreter: /usr/bin/python3

    tasks:
      - name: setup {{ runtime_user }} user
        user: name={{ runtime_user }} state=present

      - name: create deployment directories
        file: path={{ install_directory }} state=directory owner={{ runtime_user }}

      - name: create templates directory
        file: path={{ install_directory }}/templates state=directory owner={{ runtime_user }}

      - name: create templates directory
        file: path={{ install_directory }}/static state=directory owner={{ runtime_user }}

      - name: create pid directory
        file: path={{ pid_directory }} state=directory owner={{ runtime_user }}

      - name: upgrade all packages
        apt: name="*" state=latest update_cache=yes force_apt_get=yes

      - name: install needed packages
        apt: name={{ packages }} state=latest force_apt_get=yes

      - name: setup python
        command: update-alternatives --install /usr/bin/python python /usr/bin/python3 1

      - name: setup pip
        command: update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 1

      - name: Install flask
        command: pip3 install flask gunicorn

      - name: create ssl directory
        file: path=/etc/ssl/crt state=directory owner={{ runtime_user }}

      - name: create ssl directory
        file: path=/etc/ssl/csr state=directory owner={{ runtime_user }}

      - name: create ssl directory
        file: path=/etc/ssl/private state=directory owner={{ runtime_user }}

      - name: Generate an OpenSSL private key.
        openssl_privatekey:
          path: /etc/ssl/private/ansible.com.pem

      - name: generate csr
        openssl_csr:
          path: /etc/ssl/csr/ansible.com.csr
          privatekey_path: /etc/ssl/private/ansible.com.pem
          common_name: challenge.irt.fm

      - name: Generate a Self Signed OpenSSL certificate
        openssl_certificate:
          path: /etc/ssl/crt/ansible.com.crt
          privatekey_path: /etc/ssl/private/ansible.com.pem
          csr_path: /etc/ssl/csr/ansible.com.csr
          provider: selfsigned

      - name: allow authbind to run on restricted port
        file: path=/etc/authbind/byport/443 state=touch owner={{ runtime_user }} mode=0500

      - name: copy server file
        copy: src=src/server.py dest={{ install_directory }}/server.py

      - name: copy static directory
        copy: src=src/knockd.conf dest=/etc/knockd.conf

      - name: Allow everything and enable UFW
        ufw: state=enabled policy=allow

      - name: Allow port 80 by default (it will be opened with knocking)
        ufw: rule=allow port=80

      - name: Allow port 22 by default
        ufw: rule=allow port=22

      - name: Reject port 443 by default
        ufw: rule=reject port=443

      - name: update knockd service permissions
        lineinfile: path=/lib/systemd/system/knockd.service insertafter='^ProtectSystem' line='ReadWritePaths=-/etc/ufw/'

      - name: install the systemd file
        template: src=challenge.service.j2 dest=/lib/systemd/system/challenge.service
        notify:
          - reload systemd
          - restart knockd
          - restart app

    handlers:
      - name: reload systemd
        systemd: daemon_reload=yes

      - name: restart app
        systemd: name=challenge state=restarted enabled=yes

      - name: restart knockd
        systemd: name=knockd state=restarted enabled=yes
