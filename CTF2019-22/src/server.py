from flask import Flask
from flask import Flask, flash, redirect, render_template, request, session, abort, make_response
import os
 
app = Flask(__name__)
app.secret_key = os.urandom(12)

@app.route('/')
def home():
    return 'rax.io{kn0ck_on_w00d}ctf'

 
if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0', port=80)
