# Open Sesame

## description
```

[Link](https://23.253.164.48)

CTF2019-22
```

## hint
```
port knocking
```

## author
Tim Farley

## write-up

```
knock -d 10 -v 23.253.164.48 80:tcp 80:up 80:tcp
```

```
rax.io{kn0ck_on_w00d}ctf
```

## notes

.

### steps to reproduce

.

### taking it further

.

### links

* .