# Shhh

## description
```
Our R2 unit was compromised, and our incredibly
sophisticated cipher now in the hands of the Emperor!
R2-D2 has sent us a follow-up message, but
unfortunately Master Luke is still in seclusion and
is not here translate this confounded thing's
instructions! Help! We can't outguess them forever!

![luke_meme.jpg](http://bfade9c78928fb77218e-10b8dc0045fa3923b9f34ee97dd352fb.r99.cf5.rackcdn.com/CTF2019-09/luke_meme.jpg)

CTF2019-09
```

## hint
```
outguess
```

## author
Tim Farley

## write-up

Steganography is a tricky and fascinating field. Data can be encapsulated inside other file formats in almost countless ways. Tools like `SNOW` can encode data into plain text documents using prescribed patterns of spaces and tabs which, when rendered in a web browser, are almost completely transparent to the end user since web browsers completely ignore duplicate whitespace.

When hiding data inside an image file, as in this challenge, one of the most common technique involves modifying the `least significant bit` of each pixel in such a way that changes to the color palette are so incredibly subtle that the human eye cannot perceive the difference. Even if you *could* see the difference, you would probably assume that any anomalies were caused by poor image compression.

What's more, data encapsulated this way can be further compressed and encrypted so that even if discovered, it remains obscured.

Finally, even if you *know* that there is data encoded in a file, you still have to be able to reverse engineer *how* it was encoded before you can make any use of it.

For all these reasons, it's rather impractical to attack this type of challenge on a binary hex-editing level... How can you know if the encoding is using the last 1 bit, 2 bits, or even 3 bits of a byte, or if the value was changed from it's original formatting? It would take a lot longer than we have available to reverse engineer something of that level.

That said, in this challenge, we're using a meme image, so it might be interesting to try and find the original image online and compare the two files?

Let's use `exiftool` to grab some information about the image:

```
$ exiftool luke_meme.jpg | grep Image
Image Width                     : 465
Image Height                    : 379
Image Size                      : 465x379
```

A quick google search finds a very likely candidate at `https://www.google.com/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwjsgoLZhZ7hAhVn5YMKHTSzCGwQjRx6BAgBEAU&url=https%3A%2F%2Fwww.pinterest.com%2Fpin%2F442619469610628073%2F&psig=AOvVaw0uJ-mSafCFrdrAHn51Nu2o&ust=1553629081516337`

We can analyze the files using a tool like `vbindiff`, but in this scenario the files are so wildly different that picking out a pattern, while certainly plausible, would be well beyond the scope of this exercise (or my patience!)

In many cases trial and error may be the best bet. There are a lot of stego tools available, and wikipedia maintains a fairly comprehensive list. But there are a handful of really popular ones that might be worth a shot. Here are some of my favorites:

* steghide
* outguess
* binwalk

In the real world, we'd likely try a number of these tools, and probably set ourselves up with a script to test each of them in quick succession before diving into any in-depth binary analysis. Luckily, in this challenge we were provided with the phrase `outguess` in the description, so *hopefully* when we were researching various steganography tools and methodologies, this term triggered some alarms.

There's a fairly detailed write-up on the specific algorithm used by `outguess` if you desire additional reading on the subject: http://www.sigmm.org/archive/MMSec/mmsec02/outguess.pdf

Using `outguess`, we can execute the following:

```
$ outguess -r luke_meme.jpg flag.txt
Reading luke_meme.jpg....
Extracting usable bits:   32192 bits
Steg retrieve: seed: 45, len: 27
$ cat flag.txt
rax.io{sssh_1'm_h1d1ng}ctf
```

There's our flag!

## notes

I struggled with how to present this challenge. I tried to provide detailed analysis of each challenge for this CTF, and this write-up doesn't live up to my own personal standards. Still, I felt that it was an important topic to discuss and show the absolute green-field that is steganography so I decided to include the challenge but provide a pretty heavy-handed clue about the exact type of stego used.

### steps to reproduce

You can embed a text file into an image using `outguess` like so:

```
$ echo someflag > flag.txt
$ outguess -d flag.txt 6dc9ba49a954510924510f7b8f6350e6.jpg luke_meme.jpg
```
Where `luke_meme.jpg` is the output file containing your hidden information.

### taking it further

You could go so much further with something like this and be as tricky as you want. Encrypt the data and require the player to crack the hash. You could devise your own encoding algorithm and make the player reverse-engineer it manually. You could nest files inside of each other encoding them using different formats at every step. There's almost no limit to how data can be hidden in this way.

### links

* http://www.darkside.com.au/snow/
* https://security.stackexchange.com/questions/189545/steganography-outguess-vs-steghide
* https://en.wikipedia.org/wiki/Steganography_tools
* https://www.openstego.com/
* https://en.wikipedia.org/wiki/Steganography_tools
* http://www.sigmm.org/archive/MMSec/mmsec02/outguess.pdf