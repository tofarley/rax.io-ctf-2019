# Shot in the Arm

## description
```
I'm not going to lie to you. Things are looking
pretty bleak. I've provided you with a link to yet
another Imperial site. Find out what you can. A win
right now might be just the shot in the arm the Rebel
forces need to provide a healthy dose of morale for
our agents in the field. May the Force be with you.

[Link](http://104.130.219.152)

CTF2019-23
```

## hint
```
flask injection
```

## author
Tim Farley

## write-up

This is a very basic flask injection caused by improper handling of string input. Although we don't know this when we begin, the template is being rendered from a basic string format like so:

```
template = '''<strong>person.name:</strong> %s<br /><strong>person.title:</strong> %s<br /><strong>person.secret:</strong> <i>REDACTED</i>''' % (person['name'], person['title'])
```

If we assume that we may be dealing with and `injection` attack (we should always test for an injection attack) and we can see from a quick run of `curl --head` that the server is `gunicorn` which means it's a python-based application, probably flask. Knowing that flask variables are of the form `{{var_name}}` we can attempt an injection like so:

```
http://104.130.219.152/?name=Darth%20Sidious{{person.secret}}
```

And out pops our flag!

```
rax.io{1nj3ct10n}ctf
```

## notes

We're all aware of sql injections at this point, the term even appeared in a recent episode of `Star Trek: Discovery`!

But it's important to remember that injection attacks can take many forms, wherever improperly formatted input is accepted.

![sql injection](https://i.imgur.com/RdQYe4z.jpg)

### steps to reproduce

An ansible playbook is included for provisioning a server.

### taking it further

As always, this type of injection can be readily defeated by proper sanitation of input. The article linked below talks about a few other ideas that expand on these ideas, including XSS.

### links

* https://nvisium.com/blog/2015/12/07/injecting-flask.html
