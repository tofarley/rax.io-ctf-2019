from flask import Flask, request, render_template_string, render_template

app = Flask(__name__)

@app.route('/')
def hello_ssti():
	person = {'name':"unset", 'title': "unset", 'secret':"rax.io{1nj3ct10n}ctf"}
	if request.args.get('name'):
		person['name'] = request.args.get('name')
	else:
		template = '''<strong>Please specify a name</strong>'''
		return render_template_string(template, person=person)
	if request.args.get('title'):
		person['title'] = request.args.get('title')	
	template = '''<strong>person.name:</strong> %s<br /><strong>person.title:</strong> %s<br /><strong>person.secret:</strong> <i>REDACTED</i>''' % (person['name'], person['title'])
	return render_template_string(template, person=person)

if __name__ == "__main__":
	app.run(debug=True)