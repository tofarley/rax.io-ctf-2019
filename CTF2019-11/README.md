# Chatter

## description
```
We are planning a massive assault on Hoth, and we
need all the help we can get. We need to scour
unencrypted communications to find anything that
might give us edge. Classic Operational Security
(OPSEC), right? If we can just find out the version
of some software application currently in use, we
just might be able to uncover an exploit.

Remember to submit your results using the standard
format of rax.io{%s}ctf, where %s is the semantic
versioning of any software you discover in use on
planet.

[irc.pcap](http://bfade9c78928fb77218e-10b8dc0045fa3923b9f34ee97dd352fb.r99.cf5.rackcdn.com/CTF2019-11/irc.pcap)

CTF2019-11
```

## hint
```
ngrep
```

## author
Tim Farley

## write-up

This is a pretty straightforward packet capture of unencrypted Internet Relay Protocol (IRC) traffic. As the description implies, in this instance we've been tasked to investigate some basic Operational Security (OPSEC) practices. This means that we will direct our focus to the actual communications within the traffic.

A quick google search will lead us to RFC 1459 (https://tools.ietf.org/html/rfc1459) which outlines the communication protocol for IRC.

For the data stream, I turn to `ngrep`. Some people might go for `wireshark` but I'm more comfortable with ngrep. To each their own.

We know that we're looking for communications, so based on the RFC we know that we can limit the traffic to packets containing the command `PRIVMSG`:

```
ngrep -q -I irc.pcap PRIVMSG
```

the `-q` tells ngrep that we want to run quietly, i.e. we only care about the data inside the packet, not the packet itself. `-I` instructs ngrep to read from a capture file rather than a live network stream. Finally we provide `PRIVMSG` as a match expression on which to filter.

This narrows our search down significantly! Now we can focus in on the conversations. This alone is probably sufficient to find what we're looking for, but we can do better. The RFC for `PRIVMSG` states:

```
The <receiver> parameter may also be a host mask  (#mask)  or  server
mask  ($mask).   In  both cases the server will only send the PRIVMSG
to those who have a server or host matching the mask.  The mask  must
have at  least  1  (one)  "."  in it and no wildcards following the
last ".".  This requirement exists to prevent people sending messages
to  "#*"  or "$*",  which  would  broadcast  to  all  users; from
experience, this is abused more than used responsibly and properly.
Wildcards are  the  '*' and  '?'   characters.   This  extension  to
the PRIVMSG command is only available to Operators.
```

The challenge description explicitely states that we're on the hunt for *private* messages, so `#` is probably out the question. Let's see what we get?

```
$ ngrep -q -I irc.pcap "PRIVMSG\s[^#]"
input: irc.pcap
filter: ((ip || ip6) || (vlan && (ip || ip6)))
match: PRIVMSG\s[^#]

T 185.30.166.37:6667 -> 192.168.0.196:39720 [AP] #1807
  :trooper_kix!~tofarley@23-127-173-123.lightspeed.snantx.sbcglobal.net PRIVMSG trooper_jesse :-Dude, vader is such a jerk...                                                                                   

T 192.168.0.196:39720 -> 185.30.166.37:6667 [AP] #1814
  PRIVMSG trooper_kix :lol, what did he do?..                                                                                                                                                                   

T 185.30.166.37:6667 -> 192.168.0.196:39720 [AP] #1856
  :trooper_kix!~tofarley@23-127-173-123.lightspeed.snantx.sbcglobal.net PRIVMSG trooper_jesse :-Just force-choking ppl for no reason...                                                                         

T 192.168.0.196:39720 -> 185.30.166.37:6667 [AP] #1858
  PRIVMSG trooper_kix :lol..                                                                                                                                                                                    

T 185.30.166.37:6667 -> 192.168.0.196:39720 [AP] #1887
  :trooper_kix!~tofarley@23-127-173-123.lightspeed.snantx.sbcglobal.net PRIVMSG trooper_jesse :-I need to put in for a new assignment. Hoth SUCKS!..                                                            

T 192.168.0.196:39720 -> 185.30.166.37:6667 [AP] #2041
  PRIVMSG trooper_kix :https://therapydogsheal.files.wordpress.com/2010/12/dsc_0036.jpg..                                                                                                                       

T 192.168.0.196:39720 -> 185.30.166.37:6667 [AP] #2043
  PRIVMSG trooper_kix :this is you rn. lol..                                                                                                                                                                    

T 192.168.0.196:39720 -> 185.30.166.37:6667 [AP] #2074
  PRIVMSG trooper_kix :You should put in for a transfer to Endor. It's pretty chill here, and we got those new Speeder Bikes!..                                                                                 

T 185.30.166.37:6667 -> 192.168.0.196:39720 [AP] #2279
  :trooper_kix!~tofarley@23-127-173-123.lightspeed.snantx.sbcglobal.net PRIVMSG trooper_jesse :-I know. You guys get everything before we do. Did you get that new blaster firmware already?..                  

T 192.168.0.196:39720 -> 185.30.166.37:6667 [AP] #2307
  PRIVMSG trooper_kix :yeah. it's pretty sweet too. I mean. I still miss like 75% of the time, but at least now I know it's my fault. lol..                                                                     

T 185.30.166.37:6667 -> 192.168.0.196:39720 [AP] #2358
  :trooper_kix!~tofarley@23-127-173-123.lightspeed.snantx.sbcglobal.net PRIVMSG trooper_jesse :-We're still on release 1.4.2!..                                                                                 

T 192.168.0.196:39720 -> 185.30.166.37:6667 [AP] #2364
  PRIVMSG trooper_kix :ouch..                                                                                                                                                                                   

T 192.168.0.196:39720 -> 185.30.166.37:6667 [AP] #2449
  PRIVMSG trooper_kix :They got us the 1.5.1 beta and it's pretty sick, not gonna lie...                                                                                                                        

T 185.30.166.37:6667 -> 192.168.0.196:39720 [AP] #2459
  :trooper_kix!~tofarley@23-127-173-123.lightspeed.snantx.sbcglobal.net PRIVMSG trooper_jesse :-I gotta jet man. I'm going to see if I can talk to my assignment officer. I can't take it here anymore!..       

T 192.168.0.196:39720 -> 185.30.166.37:6667 [AP] #2499
  PRIVMSG trooper_kix :lol watch out for this dude! https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQRKIHf1Q0__OU4gc4kHhE9yVcs6_zKF3QP32TaDR9B0DooZAtqwQ..                                                

T 185.30.166.37:6667 -> 192.168.0.196:39720 [AP] #5754
  et 354 trooper_jesse 152 #archlinux-unregistered ~The_Pacif 45.79.102.228 asimov.freenode.net The_Pacifist H 0 :The_Pacifist..:orwell.freenode.net 354 trooper_jesse 152 #archlinux-unregistered ~mjevans li98
  4-246.members.linode.com livingstone.freenode.net mjevans H MJEvans :mjevans..:orwell.freenode.net 354 trooper_jesse 152 #archlinux-unregistered ~dtupper 216.105.171.65 wilhelm.freenode.net tupper H Tupper 
  :Got ZNC?..:orwell.freenode.net 315 trooper_jesse #archlinux-unregistered :End of /WHO list...:EMIYA!grawity@unaffiliated/grawity/bot/emiya PRIVMSG trooper_jesse :+.VERSION...   

```

That's it, we have the entire conversation right in front of us. The Rebels are planning an assault on Hoth, and trooper Jessie here has conveniently told us that Imperial troops on Hoth are still running blaster firmware version `1.4.2` which conveniently fits the semantic versioning scheme requested. The Rebellion discovered a flaw in that firmware version and exploited it to ensure that no troopers would ever hit their intended targets!

And once we add this our standard flag regalia:

```
rax.io{1.4.2}ctf
```

Alternately, because we knew we were searching for a semantic software version, we could have tried something like this:

```
$ ngrep -q -I irc.pcap "PRIVMSG.*\d+\.\d+\.\d+"
input: irc.pcap
filter: ((ip || ip6) || (vlan && (ip || ip6)))
match: PRIVMSG.*\d+\.\d+\.\d+

T 185.30.166.37:6667 -> 192.168.0.196:39720 [AP] #2358
  :trooper_kix!~tofarley@23-127-173-123.lightspeed.snantx.sbcglobal.net PRIVMSG trooper_jesse :-We're still on release 1.4.2!..                                                                                 

T 192.168.0.196:39720 -> 185.30.166.37:6667 [AP] #2449
  PRIVMSG trooper_kix :They got us the 1.5.1 beta and it's pretty sick, not gonna lie...                                                                                                                        

T 185.30.166.37:6667 -> 192.168.0.196:39720 [AP] #7228
  :hackage!mniip@haskell/bot/hackage PRIVMSG #haskell :+.ACTION mixpanel-client 0.2.0 - Mixpanel client  https://hackage.haskell.org/package/mixpanel-client-0.2.0 (domenkozar)...                              

T 185.30.166.37:6667 -> 192.168.0.196:39720 [AP] #10494
  :hackage!mniip@haskell/bot/hackage PRIVMSG #haskell :+.ACTION pandoc-citeproc 0.16.1.3 - Supports using pandoc with citeproc  https://hackage.haskell.org/package/pandoc-citeproc-0.16.1.3 (JohnMacFarlane)...
```

Of course using this method, we would have to do a little more digging to discover which of the two troopers was stationed on Hoth.


## notes

I wanted to include a challenge that focused on the operational security incident. We all find ourselves in bars, restaurants, and other public places where we don't anticipate who might overhear our conversations. Even seemlingly innocuous information that, when coupled with other innocently exposed information, can result in serious breach.

### steps to reproduce

Grab a friend and fire up a couple of IRC clients on a non-TLS encrypted connection and start chatting. Or be like me and start down the path to schitzophrenia by manning both sides of the conversation.

Fire up a tcpdump to capture traffic on the standard unencryptd irc port:

```
$ sudo tcpdump port 6667 -w irc.pcap
```

### taking it further

There's a lot more we could do here, dissecting packets via direct DCC TCP communication for chat and/or file transfers, setting up a man-in-the-middle attack against a TLS secured server and using our own server certificates to decrypt the traffic, or even just embedding fun puzzles in the IRC protocol.

### links

* https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=2ahUKEwiost33wYjhAhWm2YMKHW2lBLUQFjAAegQICBAB&url=https%3A%2F%2Fsemver.org%2F&usg=AOvVaw2wqeU7SPQk7aq7nuXGCrz-
* https://tools.ietf.org/html/rfc1459#section-4.4.1
* http://wall-skills.com/2016/ngrep-cheat-sheet-with-examples/
