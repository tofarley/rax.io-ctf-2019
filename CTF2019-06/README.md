# Message in a Bottle

## description
```
Our operatives on Alderaan managed to uncover some
interesting information before the horrific event
that transpired there. They transmitted a password
protected file off-world before time expired.
Unfortunately, there's nobody left alive who knows
the password.

The file is linked below. See what you can do.

[container.zip](http://bfade9c78928fb77218e-10b8dc0045fa3923b9f34ee97dd352fb.r99.cf5.rackcdn.com/CTF2019-06/container.zip)

CTF2019-06
```

## hint
```
zip2john
```

## author
Tim Farley

## write-up

This is the perennial password protected zip file. It is the most basic of all password cracking challenges and intended as sort of a warm-up.

The simplest way to crack the password is to extract the hash using `zip2john` and then use `john` the ripper to crack the password.

```
$ zip2john container.zip > flag.hashes
```

This creates a new file called `flag.hashes` which contains the pkzip hash for this file:

```
$ cat flag.hashes
container.zip/flag.txt:$pkzip2$1*2*2*0*25*19*4d70f69d*0*42*0*25*4d70*a5af*2ac069d6ac08f8ffdaf3ea5b992b3913c328862ba465fd6a56202174aed46544efae2714f3*$/pkzip2$:flag.txt:container.zip::container.zip
```

We can use a widely available open source tool such as John the Ripper to crack pkzip hashes. Use `john --list=formats` to see which hashing formats are compiled into your version of john. Additional formats (including some for GPU acceleration) can be included by compiling john yourself on your own platform.

```
$ john flag.hashes --show
container.zip/flag.txt:vadersucks:flag.txt:container.zip::container.zip

1 password hash cracked, 0 left
```
As we can see, no complicated masks, or wordlists required. The archive contains a file called `flag.txt` and the password `vadersucks` can be cracked from inside a virtual machine in just seconds.

Extracting the file, we can find the contents of the flag:

```
$ unzip container.zip
Archive:  container.zip
[container.zip] flag.txt password: 
 extracting: flag.txt                
$ cat flag.txt 
rax.io{dec0mpression}ctf
```

## notes

I wanted to include a dead-simple challenge like this because password-protected zip files are somewhat common in real life, and I wanted to highlight how simply putting a password on something doesn't make it secure.

### steps to reproduce

Create a file that you want to compress and populate it with a flag or other information you'd like someone to find. Add that to a password-protected archive like so:

```
$ zip -e container.zip flag.txt
Enter password: 
Verify password: 
updating: flag.txt (stored 0%)
```

### taking it further

We could expand on this challenge by making the password more difficult to crack. A more complicated password would require a more powerful CPU to crack. We could also provide the user with some information about the password which may be known from another source, such as the length of the password or the character set used, which would assist in limiting the number of passwords that we have to try.

We could further improve upon this challenge by making the zip file itself more difficult to find. The file could be embedded inside another binary executable, image, or even another compressed file! We could also hide the file inside a network stream as it was transmitted via HTTP or email, and require the player to extract the file before they can begin.

I highly recommend you upgrade to the latest version of john (1.8.0-jumbo-1 as of this writing) as I have experienced problems with the 1.79 branch that recently ships with Kali Linux not correctly parsing valid hash files.

### links

* https://countuponsecurity.files.wordpress.com/2016/09/jtr-cheat-sheet.pdf
* https://github.com/magnumripper/JohnTheRipper
* https://dfir.science/2014/07/how-to-cracking-zip-and-rar-protected.html