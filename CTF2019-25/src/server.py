# Requirements: Flask
from flask import Flask, session, request, send_from_directory


app = Flask(__name__, static_folder='static', static_url_path='')
app.config['SECRET_KEY'] = 'supersekret'


@app.route('/')
def index():
    if 'logged_in' not in session:
        session['logged_in'] = False

    if session['logged_in']:
        return '<h1>rax.io{h1jacked_s3ssi0n}ctf</h1>'
    else:
        return '<h1>Access Denied</h1>', 403

@app.route('/robots.txt')
@app.route('/web/sekret.txt')
def static_from_root():
    return send_from_directory(app.static_folder, request.path[1:])

if __name__ == '__main__':
    app.run()