# Alternate Data Sith

## description
```
Those Sith Lords are getting more devious every day.
We confiscated a drive from one of their computers.
It looks pretty normal to me, but we need a recovery 
specialist. That's where you come in. 
I've included a link to a drive image.

[ExtraStorage.img](http://bfade9c78928fb77218e-10b8dc0045fa3923b9f34ee97dd352fb.r99.cf5.rackcdn.com/CTF2019-01/ExtraStorage.img)

CTF2019-01
```

## hint
```
https://docs.microsoft.com/en-us/sysinternals/downloads/streams
```

## author
Tim Farley

## write-up

In this challenge, we are provided with a disk image with the extension `img`.

First things first:

### linux

```
$ file ExtraStorage.img 
ExtraStorage.img: DOS/MBR boot sector MS-MBR Windows 7 english at offset 0x163 "Invalid partition table" at offset 0x17b "Error loading operating system" at offset 0x19a "Missing operating system", disk signature 0xb42c9eb0; partition 1 : ID=0x7, start-CHS (0x0,2,3), end-CHS (0x22,10,42), startsector 128, 34816 sectors

$ sudo fdisk -l ./ExtraStorage.img
Disk ./ExtraStorage.img: 20 MiB, 20971520 bytes, 40960 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xb42c9eb0

Device              Boot Start   End Sectors Size Id Type
./ExtraStorage.img1        128 34943   34816  17M  7 HPFS/NTFS/exFAT
```

So we have a disk image containing an HPFS/NTFS/exFAT partition. Let's mount it up and see what we we have.

This is a full disk partition so in order to mount a specific partition, we have to target the offset.. `kpartx` does this for us.

```
$ sudo kpartx -av ExtraStorage.img
add map loop0p1 (254:0): 0 34816 linear 7:0 128
```

This will create the loop devices at `/dev/mapper/loop0p1` (loop device 0, partition 1).

```
$ sudo mkdir /mnt/win
$ sudo mount -t ntfs-3g -o ro /dev/mapper/loop0p1 /mnt/win
$ cd /mnt/win
$ ls -alh
$ ls -lah
total 8.5K
drwxrwxrwx 1 root root 4.0K Mar 14 18:38  .
drwxr-xr-x 3 root root 4.0K Mar 16 13:59  ..
drwxrwxrwx 1 root root    0 Mar 14 18:41 '$RECYCLE.BIN'
-rwxrwxrwx 1 root root   67 Mar 14 18:39  flag.txt
drwxrwxrwx 1 root root    0 Mar 14 18:15 'System Volume Information'

$ cat flag.txt 
I'm certain the Imperials left a flag somewhere in this filesystem!
```

Okay, so we have some interesting information here.

We have a `flag.txt` which taunts us, and we have a `$RECYCLE.BIN` directory that may contain some deleted files...

```
$ ls -lah
total 4.0K
drwxrwxrwx 1 root root    0 Mar 14 18:41 .
drwxrwxrwx 1 root root 4.0K Mar 14 18:38 ..
drwxrwxrwx 1 root root    0 Mar 14 18:41 S-1-5-21-1784929818-2809253712-2024181212-1001
drwxrwxrwx 1 root root    0 Mar 14 18:38 S-1-5-21-4241973204-3885494972-1262700688-1001
$ cd S-1-5-21-1784929818-2809253712-2024181212-1001/
$ ls -lah
total 512
drwxrwxrwx 1 root root   0 Mar 14 18:41 .
drwxrwxrwx 1 root root   0 Mar 14 18:41 ..
-rwxrwxrwx 1 root root 129 Mar 14 18:41 desktop.ini
$ cat desktop.ini
[.ShellClassInfo]
CLSID={645FF040-5081-101B-9F08-00AA002F954E}
LocalizedResourceName=@%SystemRoot%\system32\shell32.dll,-8964
$ cd ..
$ cd S-1-5-21-4241973204-3885494972-1262700688-1001/
$ ls -lah
total 290K
drwxrwxrwx 1 root root    0 Mar 14 18:38  .
drwxrwxrwx 1 root root    0 Mar 14 18:41  ..
-rwxrwxrwx 1 root root   62 Mar 14 18:38 '$IO864J5.exe'
-rwxrwxrwx 1 root root   58 Mar 14 18:38 '$IXEEL0O.exe'
-rwxrwxrwx 1 root root 151K Mar 14 20:04 '$RO864J5.exe'
-rwxrwxrwx 1 root root 133K Mar 14 20:04 '$RXEEL0O.exe'
-rwxrwxrwx 1 root root  129 Mar 14 20:07  desktop.ini
```

Awesome sauce. We can dig through these files with our normal usual suspects, `file`, `strings`, `xxd` but the best information we can find is a link to `http://www.sysinternals.com`.

That might be enough? If we google `sysinternals hidden files` it will lead you to a page of sysinternals utilities. We could compare the applications and figure out which one, if any, was used.

BUT, it turns out, we can also use our old friend `exiftool` -- normally used for viewing/editing EXIF data in images, to view the `properties` of a PE executable:

```
$ for file in $(ls *.exe) ; do exiftool $file | grep stream ; done
MIME Type                       : application/octet-stream
File Description                : Reveal NTFS alternate streams.
Internal Name                   : streams
Original File Name              : streams.exe
MIME Type                       : application/octet-stream
File Description                : Reveal NTFS alternate streams.
Internal Name                   : streams
Original File Name              : streams.exe
```

Aha! Now we have more or less a confirmation that this is `streams.exe` from sysinternal. Let's learn more about it:

```
The NTFS file system provides applications the ability to create alternate data streams of information. By default, all data is stored in a file's main unnamed data stream, but by using the syntax 'file:stream', you are able to read and write to alternates. Not all applications are written to access alternate streams, but you can demonstrate streams very simply. First, change to a directory on a NTFS drive from within a command prompt. Next, type 'echo hello > test:stream'. You've just created a stream named 'stream' that is associated with the file 'test'. Note that when you look at the size of test it is reported as 0, and the file looks empty when opened in any text editor. To see your stream enter 'more < test:stream' (the type command doesn't accept stream syntax so you have to use more).

NT does not come with any tools that let you see which NTFS files have streams associated with them, so I've written one myself. Streams will examine the files and directories (note that directories can also have alternate data streams) you specify and inform you of the name and sizes of any named streams it encounters within those files. Streams makes use of an undocumented native function for retrieving file stream information.
```

So we need to view the ntfs streams. We'll need to re-mount the filesystem:

```
$ sudo umount /mnt/win

$ sudo mount -t ntfs-3g -o ro,streams_interface=windows /dev/mapper/loop0p1 /mnt/win
$ sudo getfattr -R -n ntfs.streams.list /mnt/win/flag.txt
getfattr: Removing leading '/' from absolute path names
# file: mnt/win/flag.txt
ntfs.streams.list="hidden"

$ cat flag.txt:hidden
cat: 'flag.txt:hidden': No such file or directory
$ cat /mnt/win/flag.txt:hidden
rax.io{y0u_sh0uldn't_b3_h3re}ctf 
```

Here we re-mount the filesystem, enabling `streams_interface=windows` and use the `getfattr` command to find the file flag.txt contains an alternate stream called `hidden`. We can cat that file directly as `/mnt/win/flag.txt:hidden` and retrieve our flag!


### windows

The `img` file cannot be mounted using the built-in Windows 10 image mounter, but it mounts readily in most 3rd party tools, such as OSFMount (https://www.osforensics.com/tools/mount-disk-images.html)

With the image mounted as `F:` I began some basic exploration.


```
PS F:\> cat .\flag.txt
I'm certain the Imperials left a flag somewhere in this filesystem!
```

Okay then, let's look for hidden files using the Force option of `ls`:

```
PS F:\> ls -Force


    Directory: F:\


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
d--hs-        3/14/2019   7:07 PM                $RECYCLE.BIN
d--hs-        3/14/2019   5:15 PM                System Volume Information
-a----        3/14/2019   5:39 PM             67 flag.txt
```

A recyle bin! Now we're onto to something. We can use the nomenclature

```
PS F:\> cd '.\$RECYCLE.BIN\'
PS F:\$RECYCLE.BIN> ls -Force


    Directory: F:\$RECYCLE.BIN


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
d--hs-        3/14/2019   5:41 PM                S-1-5-21-1784929818-2809253712-2024181212-1001
d--hs-        3/14/2019   5:38 PM                S-1-5-21-4241973204-3885494972-1262700688-1001
```

Directory `S-1-5-21-1784929818-2809253712-2024181212-1001` contains little more than a `desktop.ini` with no interesting information, so we'll check out the other directory.

```
PS F:\$RECYCLE.BIN> cd S-1-5-21-4241973204-3885494972-1262700688-1001
PS F:\$RECYCLE.BIN\S-1-5-21-4241973204-3885494972-1262700688-1001> ls -Force


    Directory: F:\$RECYCLE.BIN\S-1-5-21-4241973204-3885494972-1262700688-1001


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
-a----        3/14/2019   5:38 PM             62 $IO864J5.exe
-a----        3/14/2019   5:38 PM             58 $IXEEL0O.exe
-a----        3/14/2019   7:04 PM         153768 $RO864J5.exe
-a----        3/14/2019   7:04 PM         135840 $RXEEL0O.exe
-a-hs-        3/14/2019   7:07 PM            129 desktop.ini


```

Binaries! But what were they before they were in the Recyle Bin? That's a good question, and a bit of a tricky one. Well, if you're feeling brave, you could try executing one with ` & '.\$RO864J5.exe'` but that's not wise unless absolutely necessary and you are on an isolated box and are prepared to accept the consequences.

It turns out there is a property called `OriginalLocation`. I quickly found a PowerShell script (https://gallery.technet.microsoft.com/scriptcenter/Get-RecycleBin-shows-the-085d12b0) that would allow me to extract this info from the Recycle Bin, however it doesn't work as-is in the because this drive isn't *technically* my recycle bin. I also found a promising program called `shellproperty` but that would require me to compile it after downloading 6GB of Visual Studio C++. What to do?

Let's try copying the files our desktop and see if there's anything we can use to examine them. We try running it through a hex editor.

Nothing pops out at me, I decided to right-click the file and check the properties.

![Screenshot](screenshot.png "Screenshot")

Bingo! The binary is called `Sysinternal Streams`. Let's see what that's about: https://docs.microsoft.com/en-us/sysinternals/downloads/streams

```
The NTFS file system provides applications the ability to create alternate data streams of information. By default, all data is stored in a file's main unnamed data stream, but by using the syntax 'file:stream', you are able to read and write to alternates. Not all applications are written to access alternate streams, but you can demonstrate streams very simply. First, change to a directory on a NTFS drive from within a command prompt. Next, type 'echo hello > test:stream'. You've just created a stream named 'stream' that is associated with the file 'test'. Note that when you look at the size of test it is reported as 0, and the file looks empty when opened in any text editor. To see your stream enter 'more < test:stream' (the type command doesn't accept stream syntax so you have to use more).

NT does not come with any tools that let you see which NTFS files have streams associated with them, so I've written one myself. Streams will examine the files and directories (note that directories can also have alternate data streams) you specify and inform you of the name and sizes of any named streams it encounters within those files. Streams makes use of an undocumented native function for retrieving file stream information.
```

So, if we hadn't figured it out before, we are dealing with Alternate Data Streams. Let's  find out!

```
PS F:\$RECYCLE.BIN\S-1-5-21-4241973204-3885494972-1262700688-1001> Get-Item -Path F:\flag.txt -Stream *


PSPath        : Microsoft.PowerShell.Core\FileSystem::F:\flag.txt::$DATA
PSParentPath  : Microsoft.PowerShell.Core\FileSystem::F:\
PSChildName   : flag.txt::$DATA
PSDrive       : F
PSProvider    : Microsoft.PowerShell.Core\FileSystem
PSIsContainer : False
FileName      : F:\flag.txt
Stream        : :$DATA
Length        : 67

PSPath        : Microsoft.PowerShell.Core\FileSystem::F:\flag.txt:hidden
PSParentPath  : Microsoft.PowerShell.Core\FileSystem::F:\
PSChildName   : flag.txt:hidden
PSDrive       : F
PSProvider    : Microsoft.PowerShell.Core\FileSystem
PSIsContainer : False
FileName      : F:\flag.txt
Stream        : hidden
Length        : 35
```

Score! The file flag.txt has a secondary stream called `hidden`!

```
PS F:\$RECYCLE.BIN\S-1-5-21-4241973204-3885494972-1262700688-1001> Get-Content -Path F:\flag.txt -Stream hidden
rax.io{y0u_sh0uldn't_b3_h3re}ctf
```

## notes

This was a really fun and interesting challenge to create! It was inspired by a previous rax.io challenge, but in that version of the challenge the creator gave the clue `ADS` which made the whole problem possible to solve and re-create in a matter of minutes. The fun came when I decided to distribute the filesystem as a disk image and attempt the same challenge in Linux, and then again in Powershell on a mounted drive. I wasn't familiar with ADS before this, so in my write-up above, I actually had to assume the mindset that I did not know it was an ADS challenge and then find a reasonable trail of clues that would help me *solve* the challenge, just as you did.

### steps to reproduce

I created a small (20MB) vmdk disk image and attached it to an Oracle VirtualBox VM running Windows 10. In the guest operating system I initialized the drive as an NTFS formatted volume.

I think created a text file using the following:

```
echo “Text that you want to be hidden” > flag.txt:hidden
```

From here you can edit `flag.txt` using your favorite hex editor.

I downloaded sysinternals `streams.exe` as a test, and then moved it to the Recycle Bin in order to provide some clues for the player. (This ended up being a slightly more challenging clue to uncover, to my surprise).

I then copied the vmdk file to a Linux system and used qemu-img to convert the vmware image to raw disk image:

```
qemu-img convert -f vmdk -O raw image.vmdk image.img
```

### taking it further

I think this is far-enough. :)

### links

* https://www.osforensics.com/tools/mount-disk-images.html
* https://gallery.technet.microsoft.com/scriptcenter/Get-RecycleBin-shows-the-085d12b0
* https://blogs.msdn.microsoft.com/matthew_van_eerde/2013/09/24/shellproperty-exe-v2-read-all-properties-on-a-file-set-properties-of-certain-non-vt_lpwstr-types/
* https://blog.malwarebytes.com/101/2015/07/introduction-to-alternate-data-streams/
* https://tm4n6.com/2018/06/22/extracting-ads-using-linux/
* https://unix.stackexchange.com/questions/86147/viewing-dll-and-exe-file-properties-attributes-via-the-command-line
* https://docs.openstack.org/image-guide/convert-images.html