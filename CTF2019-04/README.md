# Cut Off

## description
```
This image contains crucial data, but part of the
image isn't loading. Can you recover the missing data
in time?

![cut_off.png](http://bfade9c78928fb77218e-10b8dc0045fa3923b9f34ee97dd352fb.r99.cf5.rackcdn.com/CTF2019-04/cut_off.png)

CTF2019-04
```

## hint
```
IHDR header data
```

## author
Tim Farley

## write-up

![screenshot](cut_off.png)

This is a fun one. Part of the flag is clearly visible in the image, but the image has been cut in half, obscuring part of the flag. Where is the excess data? The image we were given was a peculiar size, 1920x540. Wouldn't double this height, 1920x1080 be more reasonable?

The PNG file format specification (http://www.libpng.org/pub/png/spec/1.2/PNG-Chunks.html#C.IHDR) outlines the use of the `IHDR` chunk, which must be the first chunk of the file and must contain the following:

```   
   Width:              4 bytes
   Height:             4 bytes
   Bit depth:          1 byte
   Color type:         1 byte
   Compression method: 1 byte
   Filter method:      1 byte
   Interlace method:   1 byte
```

In addition there is an associated CRC value per chunk inside the PNG file.

If none of this interests you, there's a fantastic little windows utility that will allow you to make this change with a nice GUI. Try opening the file and setting the height to 1080 and see what happens:

![screenshot](screenshot.png)

But we demand more than that, right? We want to understand how this works. We could go read through the PNG file format spec but I think it will be more fun to explore this ourselves. Open up your favorite image editor and create a an image with a blank white canvas. 1920x1080 pixels in size. It doesn't really matter how many pixels per inch you use or any other settings. We just want to see what changes when we resize an image.

Now resize the image to 1920x540 and use a tool like `vbindiff` to check out the differences in the binary:

```
$ vbindiff halfsize.png fullsize.png
```

![screenshot](vbindiff.png)

We see the values `021C` (540 in decimal) in halfsize.png and `0438` (1080 decimal) in fullsize.png. This is what we're looking for!

We can go back to our regular challenge use a hex editor to replace `021C` with `0438`, but we still have to account for the checksum. In this example, we know the correct checksum because we have the original file on hand, but for the challenge, we will need to calculate what the checksum *should* be.

We can let a tool like `pngcheck` calculate the checksum for us:

```
$ pngcheck halfsize.png
halfsize.png  CRC error in chunk IHDR (computed e8d3c143, expected 173eb359)
ERROR: halfsize.png
```

In this case, we would replace the values `173eb359` in our image with the calculated `e8d3c143`.

The W3C provides the detailed math for calculating the CRC (https://www.w3.org/TR/PNG-Structure.html) along with sample code for PNG CNC calculation: (https://www.w3.org/TR/PNG-CRCAppendix.html)

So, let's use that code snippet provided by W3C. Download that code and add the following function for main:

```
int main()
   {
       unsigned char data[] = "\x49\x48\x44\x52\x00\x00\x07\x80\x00\x00\x04\x38\x08\x06\x00\x00\x00";
       printf("%lx\n", crc(data, 17));
       return 0;
   }
```

The `unsigned char data[]` variable is the 17 bytes. Starting with `IHDR` and including the next 13 bytes (as outlined above) for a total of 17.

Compile and run this code:
```
$ gcc -o crc crc.c 
$ ./crc
e8d3c143
```

This matches everything we've seen so far! Update the CRC block in your image file and grab the flag!

```
rax.io{cut_in_half_lolol}ctf
```

## notes

I came across a challenge similar to this one in a recent CTF and I wasn't able to solve it in the alloted time. After the CTF I made sure to go back through until I understood it thoroughly, so I thought it would be a good exercise for myself to re-create it here.

### steps to reproduce

The easiest way to reproduce is to use `tweakpng` to make changes to an image.

### taking it further

We could add additional corruption to the file making it harder to open and requiring a deeper dive on the PNG file format.

### links

* http://www.libpng.org/pub/png/spec/1.2/PNG-Chunks.html#C.IHDR
* http://entropymine.com/jason/tweakpng/
* http://www.libpng.org/pub/png/apps/pngcheck.html
* https://www.w3.org/TR/PNG-CRCAppendix.html
* https://www.w3.org/TR/PNG-Structure.html
