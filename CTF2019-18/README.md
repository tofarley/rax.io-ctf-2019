# Balance of the Force

## description
```
*You sense an unbalance in the force*  

Young Jedi, we need you to review all material
printed for the Jedi High Council meetings.  We have
a new batch of stickers ready for your approval. 
Please review this image for any imperfections.

Only you can bring order and balance to our galaxy.

[raxio_CTF_2019-rg.svg](http://bfade9c78928fb77218e-10b8dc0045fa3923b9f34ee97dd352fb.r99.cf5.rackcdn.com/CTF2019-18/raxio_CTF_2019-rg.svg)

CTF2019-18 
```

## hint
```
Some images should be opened with a text editor.
```

## author
Towne Besel

## write-up

```
rax.io{h1dd3n_in_plAIn_s1ght}ctf
```

## notes

blah

### steps to reproduce

blah

### taking it further

blah

### links

blah