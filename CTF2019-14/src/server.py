from flask import Flask
from flask import Flask, flash, redirect, render_template, request, session, abort, make_response
import os
 
app = Flask(__name__)
app.secret_key = os.urandom(12)

@app.route('/')
def home():
    if request.cookies.get('logged_in') == 'True':
        resp = make_response(render_template('loggedin.html'))
    else:
        resp = make_response(render_template('login.html'))
        resp.set_cookie('logged_in', 'False')
    return resp

 
@app.route('/login', methods=['POST'])
def do_admin_login():
    if request.form['password'] == 'ThisIsACrazyPassword' and request.form['username'] == 'lord_vader':
        resp = make_response(render_template('loggedin.html'))
        resp.set_cookie('logged_in', 'True')
        session['logged_in'] = True
        return resp 
    else:
        flash('wrong password!')
    return home()
 
if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0', port=80)
