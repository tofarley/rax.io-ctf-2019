# Quotables

## description
```
We've got a strange one. We picked up what appears to
be random traffic full of... quotes from both Rebel
and Imperial forces alike. It's as though someone has
been listening to our conversations for the last
forty years for their own sadistic enjoyment. See if
you can make heads or tails out of this mess.

[datastream.pcap](http://bfade9c78928fb77218e-10b8dc0045fa3923b9f34ee97dd352fb.r99.cf5.rackcdn.com/CTF2019-10/datastream.pcap)

CTF2019-10
```

## hint
```
look for patterns
```

## author
Tim Farley

## write-up

This one is intended to test your pattern recognition. While this may seem silly at first, it is a common packet-inspection technique required when evaluating traffic for anomalies.

Let's take a quick look at the data:

```
$ ngrep -q -I datastream.pcap | head -20
input: datastream.pcap
filter: ((ip || ip6) || (vlan && (ip || ip6)))

T 192.237.180.11:8002 -> 192.168.0.196:40860 [AP] #4
  There's always a bigger fish..                                                                                                                                                                             

T 192.237.180.11:8000 -> 192.168.0.196:48228 [AP] #12
  Look, I can't get involved. I've got work to do. It's not that I like the Empire; I hate it, but there's nothing I can do about it right now... It's all such a long way from here..                       

T 192.237.180.11:8001 -> 192.168.0.196:38410 [AP] #20
  Only a Sith Lord deals in absolutes..                                                                                                                                                                      

T 192.237.180.11:8001 -> 192.168.0.196:38412 [AP] #28
  Some day you're gonna be wrong, I just hope I'm there to see it..                                                                                                                                          

T 192.237.180.11:8001 -> 192.168.0.196:38414 [AP] #36
  R2-D2, you know better than to trust a strange computer..                                                                                                                                                  

T 192.237.180.11:8000 -> 192.168.0.196:48236 [AP] #44
  Congratulations. You are being rescued. Please do not resist..          
```

There's a lot we can determine a lot of information at a quick glance. We can see that all of the traffic is TCP. We can also note that all of the traffic originates and terminates at the same pair of IP addresses, and the traffic all flows in one direction across four ports (8000-8003).

A quick check fo the timestamps reveals that the traffic occured at fairly regular intervals, roughly two seconds apart. We could also check the output for any binary encoded data using `-xX` but that is not the case here.

We can also see that the data in these packets conains little more than a list of famous quotes from Star Wars movies, so we'll discount that and focus on the packets themselves.

The only variation we really see here is the changing port range, so let's limit our results and focus on those:

```
$ ngrep -q -I datastream.pcap | grep -oP 'T\s\d+\.\d+\.\d+\.\d+:\K(\d+)' | head -30
8002 
8000 
8001 
8001 
8001 
8000 
8000 
8003 
8001 
8000 
8002 
8000 
8001 
8003 
8001 
8000 
8000 
8000 
8000 
8001 
8002 
8000 
8001 
8001 
8001 
8001 
8003 
8000 
8000 
8000 
```

Here we've restricted the output to show us only the host port by using a little grep-fu. the `\K(\d+)` portion of the grep identifies a capture group of data that we want grep to return. Specifically, this grep captures any line that begins with a literal `T`, followed by any number of spaces and an `IP:PORT` combination, and then it returns only the specified port. 

As we look over the entire document, it will hopefully become clear to us that there are generally 9 transactions between every request to port `8002` and in those nine transactions, a possible request to port `8003`. Either of these could be worthy anomolies to investigate, but let's filter on the former and filter out port `8003` completely.

```
ngrep -q -I datastream.pcap portrange 8000-8002 | grep -oP 'T\s\d+\.\d+\.\d+\.\d+:\K(\d+)' | head -30
```

Now we're getting somewhere! With port `8003` out of the way, we can readily identify a pattern of 8 consecitive requests to ports 8000 and 8001 between every 8002. Perhaps 8002 signifies the start of a byte... And if that's the case, numbers 8000 and 8001 look awfully similar to binary when considering only the list significant digit.

Let's modify our grep so that we only return ports `8000-8001` and update the capture group to retrieve only the last digit of the port number:

```
$ ngrep -q -I datastream.pcap portrange 8000-8001 | grep -oP 'T\s\d+\.\d+\.\d+\.\d+:\d+\K(\d)' | tr -d '\n'
0111001001100001011110000010111001101001011011110111101101110111001100110110110001100011011011110110110101100101010111110111010000110000010111110111010001101000001100110101111101100100010000000111001001101011010111110111001100110001011001000011001101111101011000110111010001100110
```

I also went ahead and removed the newlines using `tr`. The result looks suspiciously like a binary string to me.

```
$ python3
Python 3.6.6 (default, Jun 27 2018, 14:44:17) 
[GCC 8.1.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import binascii
>>> n = int('0111001001100001011110000010111001101001011011110111101101110111001100110110110001100011011011110110110101100101010111110111010000110000010111110111010001101000001100110101111101100100010000000111001001101011010111110111001100110001011001000011001101111101011000110111010001100110', 2)
>>> n.to_bytes((n.bit_length() + 7) // 8, 'big').decode()
'rax.io{w3lcome_t0_th3_d@rk_s1d3}ctf'
```

The above is a bit of a convoluted way to convert the ascii. You can easily use a utility such as CyberChef to do the conversion for you. Nobody will think less of you. In fact, I would think more highly of you for being effecient and not spending valuable time trolling stackoverflow for a good conversion snippet, as I did ;)

## notes

I was inspired by a recent event in the Fireshell CTF where we were provided a packet capture and asked to find the flag. It turned out the flag was hidden in the traffic by using the `evil bit` to indicate packets that shoule be interpretted as a a `1`. I never solved the challenge, and I was initially annoyed, thinking the challenge was irreverent. Later I came to appreciate the specific type of analytical challenge it propsed, and I wanted to include something similar, albeit simpler, in this event.

### steps to reproduce

The servers are really just simple systemd services running netcat on repeat:

```
/usr/bin/shuf -n1 /home/tofarley/starwars_quotes.txt | nc -l 8000
```

I configured the systemd service to ignore rapid-restarts and then fired off a local bash script to connect to each port in the order I intended by encoding the flag into binary. I used tcpdump to capture traffic on ports 8000-8003.

```
sudo tcpdump -w datastream.pcap portrange 8000-8003
```

See the file `src/s8000.service` for an example of of this systemd configuration.

I have also included `src/starwars_quotes.txt` and `src/starwords.sh` which contain the list of quotes used and the script to generate the pattern.

All of this is just sort of hacked together, but I included the code in this repository for the sake of completion.

### taking it further

Packet analysis is a huge topic and there are literally thousands of ways you can expand on something like this.

### links

* https://ctftime.org/writeup/12954
* https://www.malware-traffic-analysis.net/training-exercises.html
* https://stackoverflow.com/questions/7396849/convert-binary-to-ascii-and-vice-versa
