# Jedi Council email delivery

## description
```
Jedi Commander, the Council's email system is rejecting messages from one of our contractors, "irt.fm".  We think it has something to do with some record used for verifying email. We need you to find out why these emails are getting bounced and you better hurry, we are expecting to receive vital plans for our mission.

CTF2019-21
```

## hint
```
https://en.wikipedia.org/wiki/Sender_Policy_Framework
```

## author
Towne Besel

## write-up

```
dig any irt.fm | grep spf
```
irt.fm.         300 IN  TXT "v=spf1 include:rax.io{catch_me_if_you_can}ctf"```
```

## notes

The participant is provided domain "irt.fm" and asked to check for email issues.  We configured an SPF record for the domain which causes receiving email servers to reject messages.  SPF records are configured as a single line in the TXT record of the DNS for the domain.   

Using SPF record syntax allows for delivery of hint through "email issues".  Websites that check for valid SPF records will return the flag. See links below for examples. 

### steps to reproduce

Add a TXT record for the target domain.  

### taking it further

{add picture of failed SPF check from https://www.dmarcanalyzer.com/spf/checker/}

### links

https://mxtoolbox.com/SuperTool.aspx?action=spf%3airt.fm&run=toolpage
https://www.dmarcanalyzer.com/spf/checker/
https://dnslytics.com/spf-lookup/irt.fm