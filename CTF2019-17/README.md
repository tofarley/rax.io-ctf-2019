# Probing for information

## description
```
It's important to recon a target and find out what
you can before you begin. I have linked you to an
Imperial-owned web server. There may be some useful
information we can glean about their infrastructure.

[Link](http://23.253.203.36)

CTF2019-17
```

## hint
```
check your headers
```

## author
--

## write-up

```
$ curl --head http://23.253.203.36
HTTP/1.1 200 OK
Date: Tue, 19 Mar 2019 01:02:58 GMT
Server: Apache httpd 2.4.18 ((Ubuntu)) --> The flag is at ./banner_flag.txt
Last-Modified: Tue, 19 Mar 2019 00:55:38 GMT
ETag: "189-58467f6ab983e"
Accept-Ranges: bytes
Vary: Accept-Encoding
Content-Type: text/html
Content-Length: 393
Age: 1767
Via: 1.1 ord1fwsg02-wcg.security.rackspace.net
```

Navigate to `http://23.253.203.36/banner_flag.txt` to find our flag:

```
rax.io{abp:always_be_probing}ctf
```

## notes

This challenge was re-hashed from a previous rax.io CTF event. I liked that it was a simple challenge that requires you to look at the apache banner.

### steps to reproduce

This challenge is available in the TVA github repo (https://github.rackspace.com/TVA/rackspace_ctf/tree/master/CTF-09)

### taking it further

...

### links

* https://www.hackingarticles.in/5-ways-banner-grabbing/