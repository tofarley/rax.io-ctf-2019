# Bothan Spies

## description
```
Many Bothan's died to bring us this information.
The Empire was smart enough to BitLocker encrypt
the plans to the second Death Star. The Bothan's
did their job to get us the data, it is your
job to decrypt it.

[Encrypted.img](http://bfade9c78928fb77218e-10b8dc0045fa3923b9f34ee97dd352fb.r99.cf5.rackcdn.com/CTF2019-02/Encrypted.img)

CTF2019-02
```

## hint
```
bitlocker2john
```

## author
Tim Farley

## write-up

You are provided a disk image `Encrypted.img`

We can confirm that the image is truly an NTFS-formatted disk:

```
$ fdisk -l Encrypted.img
Disk Encrypted.img: 78.3 MiB, 82051072 bytes, 160256 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xe525c261

Device         Boot Start    End Sectors Size Id Type
Encrypted.img1        128 155775  155648  76M  7 HPFS/NTFS/exFAT
```

We can use one of the ancillary tools that ships with john the ripper to generate the hashes:

```
$ bitlocker2john -i Encrypted.img
<<TRUNCATED>>
User Password hash:
$bitlocker$0$16$78f0d415f3962e3654f105c74c2d9be7$1048576$12$d0b0ec5987dfd4010e000000$60$10ac3ffb38586893397f0a8b0e80e31c2aa0d127e21b298e2989a443352c1876d794248383c043ae7ba346198b35d66ca78013d2db71e25cefe48ea3
Hash type: User Password with MAC verification (slower solution, no false positives)
$bitlocker$1$16$78f0d415f3962e3654f105c74c2d9be7$1048576$12$d0b0ec5987dfd4010e000000$60$10ac3ffb38586893397f0a8b0e80e31c2aa0d127e21b298e2989a443352c1876d794248383c043ae7ba346198b35d66ca78013d2db71e25cefe48ea3
Hash type: Recovery Password fast attack
$bitlocker$2$16$4fb61ac0c2b4a1632915c26452ea6f24$1048576$12$607fe2f6bddad40106000000$60$85463754b364edc856caf2587fe50c1c41dc00e29d0cae7e8e186747b26e4b7c00afdf46bef39287318402cb166aba314210a8b75ae517e483b836da
Hash type: Recovery Password with MAC verification (slower solution, no false positives)
$bitlocker$3$16$4fb61ac0c2b4a1632915c26452ea6f24$1048576$12$607fe2f6bddad40106000000$60$85463754b364edc856caf2587fe50c1c41dc00e29d0cae7e8e186747b26e4b7c00afdf46bef39287318402cb166aba314210a8b75ae517e483b836da
```

Bitlocker will always yield 4 hashes like this. The first two are hashes for the user password (using two different types of verification) and the last two are for the Recovery password.

We'll grab the first hash in the list and stuff it into a file called `bitlocker.hash` and do a dictionary attack.

```
john --format=bitlocker --wordlist=/usr/share/wordlists/rockyou.txt bitlocker.hash
Using default input encoding: UTF-8
Loaded 1 password hash (BitLocker, BitLocker [SHA-256 AES 64/64])
Cost 1 (iteration count) is 1048576 for all loaded hashes
Will run 2 OpenMP threads
Note: This format may emit false positives, so it will keep trying even after
finding a possible candidate.
Press 'q' or Ctrl-C to abort, almost any other key for status
princess         (?)
1g 0:00:26:12 0.05% (ETA: 2019-04-29 10:06) 0.000635g/s 5.107p/s 5.107c/s 5.107C/s renault..pumkin
Session aborted
```

Bingo! The password is `princess`.

Let's mount the volume and see what we find inside!

```
$ mkdir -o /mnt/win /mnt/bitlocker
$ sudo kpartx -av Encrypted.img
add map loop0p1 (254:0): 0 155648 linear 7:0 128
$ sudo dislocker -r -V /dev/mapper/loop0p1 -uprincess -- /mnt/bitlocker/
$ sudo mount -r -o loop /mnt/bitlocker/dislocker-file /mnt/win
$ cat /mnt/win/flag.txt
rax.io{n0t_encypted_n0w!}ctf
```

## notes

This was inspired by one of the challenges from a previous rax.io, but in that version they gave a riddle that would help you guess the password. I decided to go about having the player do a dictionary attack against a simple password. I really wanted to give some information about a partial password so that users could play around with john's `--mask` options, but that turned out to be really slow unless you are able to run on CUDA/OpenCL and I couldn't assume that anyone had access to this during the event.

### steps to reproduce

I created a small (80MB) vmdk disk image (bitlocker requires a minimum of ~64MB) and attached it to an Oracle VirtualBox VM running Windows 10. In the guest operating system I initialized the drive as an NTFS formatted volume, created a `flag.txt` file and encrypted the volume using bitlocker with a simple password that I knew was in `rockyou.txt`.

I then copied the vmdk file to a Linux system and used qemu-img to convert the vmware image to raw disk image:

```
qemu-img convert -f vmdk -O raw image.vmdk image.img
```

### taking it further

Check out the links below for information on obfuscating strings in C.

### links

* https://openwall.info/wiki/john/OpenCL-BitLocker
* https://www.ceos3c.com/open-source/open-bitlocker-drive-linux/
* https://github.com/e-ago/bitcracker