# What's his Name?

## description
```
Old Ben Kenobi? He's just a nice old man. How did he
get mixed up in all of this?

[old_ben.jpg](http://bfade9c78928fb77218e-10b8dc0045fa3923b9f34ee97dd352fb.r99.cf5.rackcdn.com/CTF2019-12/cmF4Lmlve2MwbnZlcnNpb259Y3Rm.jpg)

CTF2019-12
```

## hint
```
base64
```

## author
Tim Farley

## write-up

This one is just having fun with a very harsh Star Wars reality. You can inspect the image all you want, but there's nothing particularly interesting about it. Both the meme caption and the description mention a `name` though, and if you pay attention to the name of the file, it looks suspiciously like `base64` encoding minus the telltale `==`. It also doesn't match the simple `*_meme.jpg` filenames we've seen in the other image challenges.

Let's have a look:

```
$ s=cmF4Lmlve2MwbnZlcnNpb259Y3Rm.jpg
tofarley@kali:~/rax.io_ctf_2019/whats_his_name$ echo ${s%.*} | base64 -d
rax.io{c0nversion}ctf
``` 

## notes

I saw a challenge like this in a recent CTF where we were directed to a URL via QR code, and the URL was encoded this way. I think it's a good reminder that you can't take *anything* for granted.

### steps to reproduce

Base64 encode a string and rename the file:

```
$ s=$(echo "somestring" | base64)
$ mv old_ben.jpg $s.jpg

```

### taking it further

I'm sure there's more we can do with this one, but let's not focus on whether we can, and focus on whether we should.

### links

* https://en.wikipedia.org/wiki/Base64
