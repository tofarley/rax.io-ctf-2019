#!/usr/bin/env bash

echo "CREATING VIRTUALENV AT $HOME/.virtualenvs/ctf"
python -m virtualenv $HOME/.virtualenvs/ctf

. $HOME/.virtualenvs/ctf/bin/activate

echo "INSTALLING ANSIBLE, PYRAX, AND PWSAFE MODULES INTO VIRTUALENV."
$HOME/.virtualenvs/ctf/bin/pip -q install ansible pyrax git+ssh://git@github.rackspace.com/O3Eng/pwsafe.git#egg=pwsafe

mkdir -p $HOME/.virtualenvs/ctf/share/inventory

echo "====="
echo "CREATING DEFAULT INVENTORY.INI at $HOME/.virtualenvs/ctf/share/inventory/inventory.ini"
cat > $HOME/.virtualenvs/ctf/share/inventory/inventory.ini << EOL
[localhost]
localhost ansible_connection=local ansible_python_interpreter=~/.virtualenvs/ctf/bin/python
EOL

echo "====="
echo "DOWNLOADING rax.py inventory file for ansible"
curl -s -o $HOME/.virtualenvs/ctf/share/inventory/rax.py "https://raw.githubusercontent.com/rackerlabs/developer.rackspace.com/master/deploy/inventory/site/rax.py"
chmod +x $HOME/.virtualenvs/ctf/share/inventory/rax.py

echo "====="
echo "DOWNLOADING THE rack CLI"
curl -s -o $HOME/.virtualenvs/ctf/bin/rack "https://ec4a542dbf90c03b9f75-b342aba65414ad802720b41e8159cf45.ssl.cf5.rackcdn.com/1.2/Linux/amd64/rack"
chmod +x $HOME/.virtualenvs/ctf/bin/rack 

echo "====="
echo "CONFIGURING YOUR RACKSPACE CLOUD USER ACCOUNT"

echo -e "Rackspace Cloud Username: "
read username
echo -e "Rackspace Cloud API key (Under My Profile and Settings): "
read apikey
echo -e "Region (ORD/DFW/IAD/etc): "
read region

mkdir -p $HOME/.rack

cat > $HOME/.rack/config << EOL
username = ${username}
api-key = ${apikey}
region = ${region}

EOL

echo "export RAX_CREDS_FILE=$HOME/.rack/config" >> $HOME/.bashrc

#cat > $HOME/.rackspace_cloud_credentials << EOL
#[ansible]
#username = ${username}
#api-key = ${apikey}
#region = ${region}

#EOL

# Ansible and the rack command use the same pyrax format, but look in different places.
# We'll just create a symlink to keep everything synced up.
#rm ~/.rack/config
#ln -s ~/.rackspace_cloud_credentials ~/.rack/config

echo 'Ansible has been configured for use in the `ctf` virtualenv'
echo 'Run `source $HOME/.virtualenvs/ctf/bin/activate` to use.' 
