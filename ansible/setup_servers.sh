#!/usr/bin/env bash

$HOME/.virtualenvs/ctf/bin/ansible localhost -i $HOME/.virtualenvs/ctf/share/inventory/inventory.ini -m rax -a "name=test-%02d key_name=id_rsa_rax flavor=4 image=6e731e8e-ff3d-4e27-be6c-147fde688b7c wait=yes region=ORD count=2 exact_count=yes group=test"
