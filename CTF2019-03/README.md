# Who Goes There?

## description
```
The Empire is using advanced technology to restrict
access to this site. If we can't get onto this server
soon, millions of innocent Gungan's will suffer at
the hands of the Emperor!

[Link](http://23.253.216.44/)

CTF2019-03
```

## hint
```
user-agent
```

## author
Tim Farley

## write-up

Navigating to the web page reveals the following information:

```
$ curl http://23.253.216.44/
This site must be viewed using an Imperial-issued BlackBerry PlayBook 2.1
```

This sounds like some `user-agent` spoofing is in order.

We can accomplish this by finding out what the user-agent string is for a BlackBerry PlayBook 2.1. A quick google search reveals some possibilities:

```
$ curl -H "User-Agent: Mozilla/5.0 (PlayBook; U; RIM Tablet OS 2.1.0; en-US) AppleWebKit/536.2+ ( KHTML, like Gecko) Version/7.2.1.0 Safari/536.2+" http://23.253.216.44/
rax.io{sp00fd}ctf
```

Another way to accomplish this is using your web browser's `Developer Tools`. In Chrome, open the `Developer Tools` and navigate to the ellipses, then `More Tools / Network Conditions`. Here you can set and override the User agent. BlackBerry PlayBook 2.1 is one of the available presets.

## notes

This is the oldest trick in the book!

### steps to reproduce

This was a simple Flask app hosted with gunicorn. The code, and a sample systemd service file, are included in the `src/` directory.

An ansible playbook is provided for use when building this challenge.

### taking it further



### links

* https://www.cyberciti.biz/faq/curl-set-user-agent-command-linux-unix/
* http://www.webapps-online.com/online-tools/user-agent-strings/dv/brand573348/blackberry-playbook
* https://developers.whatismybrowser.com/useragents/explore/
* https://www.programcreek.com/python/example/72648/flask.request.user_agent