from flask import Flask, Response, request
import re

app = Flask(__name__)

@app.route('/')
def hello_world():
    if request.user_agent.platform == 'blackberry' and re.search('PlayBook', request.user_agent.string):
        return 'rax.io{sp00fd}ctf'
    else:
        return 'This site must be viewed using an Imperial-issued BlackBerry PlayBook 2.1'