# A Message from Afar

## description
```
R2 units can do a lot more than just project images.
They have been known to embed secret codes inside the
data. This just came across the wire from R2-D2. See
if you can find anything interesting inside. Link
below.

[yoda_meme.jpg](http://bfade9c78928fb77218e-10b8dc0045fa3923b9f34ee97dd352fb.r99.cf5.rackcdn.com/CTF2019-08/yoda_meme.jpg)

CTF2019-08
```

## hint
```
rot13
```

## author
Tim Farley

## write-up

This one is pretty straightforward. We are provided with the following image:

[alt text](yoda_meme.jpg "Yoda Meme")

In lieu of any expected trickery involving the actual downloading of a given file, it's always a good idea to check the file for any obvious strings:

```
$ strings yoga_meme.jpg
<<TRUNCATED>>
O>'X
V)eqw
>(i>
-mld
g{kxb*
W|p4
!MNrT
enk.vb{gu3_e3oryyv0a_1f_j1aavat!}pgs
```

This looks promising! Knowing that our standard flag format is `rax.io{%s}ctf` we can see a string at the very end of the file which matches this pattern:

```
$ strings yoga_meme.jpg | grep -oP '.{6}\{.*\}.{3}'
enk.vb{gu3_e3oryyv0a_1f_j1aavat!}pgs
```

Obviously this is our flag, but it has been encoded in some way. There are many tools to check encodings, but I'm fond of CyberChef (https://gchq.github.io/CyberChef/). CyberChef is a web app that allows us to quickly test and stack various "recipes" for data conversion. CyberChef even has a nice `Magic` conversion that will attempt to find a solution using various encoding techniques. In this instance, Magic doesn't want to decode it, but what do we know about the string?

Well, the first character should be an `r` and it's currently an `e`. The braces appear to be unchanged.

It's likely that this is some sort of basic substitution cipher, but which one? There's no easy way to accurately identify a cipher 100% of the time, so our best bet is a little trial and error. We can start playing with various cipher decryption tools and see what we come up with.

We tried sites like https://quipqiup.com/ and they yielded some interesting (if not entirely accurate) results.

```
ras.io{tv3_r3pulli0n_1y_m1nning!}ctf
```

Close, but unless we have a really warped expectation of l33t speak, and x'x can magically become s's, this won't do.

We can land on a page such as rumkin.com to find a bunch of common ciphers, and we can feed them into various tools. In this case, I settled on my favorite, CyberChef.

We filter down the list of available converters by typing `cipher` into the search box, and when we apply the `ROT13` encoder, we find our answer!

```
rax.io{th3_r3belli0n_1s_w1nning!}ctf
```

## notes

This was actually based on a challenge from the last rax.io CTF. In the previous event, the flag was not encoded and was easily viewable with strings. Since I had already created a new challenge that was solvable in this manner, I decided to spice this one up a bit.

### steps to reproduce

Open the image file in the hex editor of your choice and append some hex data to the end.

Convert the string to suit your needs using a tool such as CyberChef or the command below:

```
$ echo 'rax.io{th3_r3belli0n_1s_w1nning!}ctf' | tr 'A-Za-z' 'N-ZA-Mn-za-m' | xxd -p -u
656E6B2E76627B6775335F65336F7279797630615F31665F6A3161617661
74217D7067730A
```

This uses linux translate command `tr` to map each character of the string to a new character, rot13 style, and then converts the output to hexadecimal for appending to the image.

This, of course, destroys any built-in CRC for the image, but very few image viewers bother to honor it anyway.

### taking it further

There's so much you can do with a challenge like this. While this technically counts as steganography, this is the absolute most basic form, and this style would not work for other types of formats other than images, which have stronger validation.

### links

* http://rumkin.com/tools/cipher/
* http://practicalcryptography.com/cryptanalysis/text-characterisation/identifying-unknown-ciphers/
